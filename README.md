# Travail pratique Base de données II

Travail pratique final du cours de Base de données II en 2e année de la Techniques de l'informatique de gestion du Cégep de Drummondville.

## Version

1.0.0

## Année

2018

## Type de programme

C'est un gestionnaire de BD qui permets de gérer un club d'imprimerie 3D.

## Guide d'installation

Ces instructions vont vous permettre d'obtenir une copie du projet fonctionnel sur votre machine locale.

### Prérequis

Un ordinateur avec le système d'exploitation Windows.

```
Windows 10
Windows 8
Windows 7
```

### Installation

```
Cloner le projet dans le dossier désiré

```
Git clone https://gitlab.com/j_lemaire/TP2BD2.git

## Auteurs

**Joël Lemaire-Younoussa**

## License

Ce projet est  sous la licence MIT.