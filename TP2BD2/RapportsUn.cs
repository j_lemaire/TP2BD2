﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP2BD2.Gestion;

namespace TP2BD2
{
    /// Question D1
    /// Choisir la création
    /// <summary>
    /// Classe permettant d'afficher tous les essais ayant été fait pour une création. 
    /// </summary>
    class RapportsUn
    {
        protected Vue _vue;
        protected Constantes _constantes;
        protected GestionCreation _creation;

        /// <summary>
        /// Constructeur pour RapportsUn
        /// </summary>
        public RapportsUn()
        {
            _vue = new Vue();
            _constantes = new Constantes();
            _creation = new GestionCreation();
        }

        /// <summary>
        /// Menu pour chosir la création et afficher le rapport
        /// </summary>
        public void MenuPrincipal()
        {
            int creationId;
            List<int> listeCreation = new List<int>();
            listeCreation = _creation.ObtenirListe();
            if (!_vue.ListeVide(listeCreation))
            {
                creationId = _vue.ChoisirOption(listeCreation, _constantes.creationId);
                Console.Clear();
                _creation.AfficherUsagers(creationId);
                _creation.AfficherImpressions(creationId);
                _creation.AfficherFilaments(creationId);
                _creation.AfficherEssaies(creationId);
                _creation.AfficherImprimantes(creationId);
                _creation.AfficherQuantite(creationId);
                Console.Write("\nFin du rapport. \nPressez sur entrée --->");
                Console.Read();
            }
            return;
        }
    }
}
