﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe permettant d'accèder aux EssaieImpressions.
    /// </summary>
    class ImpressionClee
    {

        /// <summary>
        /// Constructeur de EssaieImpression
        /// </summary>
        public ImpressionClee()
        {

        }

        /// <summary>
        /// Obtient les Id des impressions dans la table Impression.
        /// </summary>
        /// <returns>Une liste avec les Id des impressions de Impression</returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var impressions = context.Impressions;
                foreach (var Impressions in impressions)
                {
                    Console.WriteLine("\nID: {0} | Nom: {1}",
                                    Impressions.ID,
                                    Impressions.Nom.Trim()
                                    );

                    listeId.Add(Impressions.ID);
                }
            }
            return listeId;
        }

        /// <summary>
        /// Permet de vérifier si une ou des impressions existent pour un usager.
        /// </summary>
        /// <param name="usagerId">L'id de l'usager</param>
        /// <param name="doitAfficher">True si l'on doit afficher les relations</param>
        /// <returns></returns>
        public bool Existe(int usagerId, bool doitAfficher)
        {
            using (var context = new TP2BD21282433Entities())
            {
                var impression = context.Impressions
                                    .Where(c => c.UsagerId == usagerId).ToList();
                ;
                if (impression.Count > 0)
                {
                    if (doitAfficher)
                    {
                        Afficher(impression);
                    }
                    return true;
                }
                else return false;
            }
        }

        /// <summary>
        /// Permet d'afficher les Impressions.
        /// </summary>
        /// <param name="impression">Une liste d'impressions</param>
        public void Afficher(List<Impression> impression)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\n Les Impressions suivantes seront supprimées:");
            foreach (Impression imp in impression)
            {
                Console.WriteLine("\n Impression ID: {0} | Nom: {1}",
                        imp.ID,
                        imp.Nom.Trim());
            }
            Console.ResetColor();
        }

        /// <summary>
        /// Permet de vérifier si un essaie existe pour un usager.
        /// </summary>
        /// <param name="usagerId">L'id de l'usager</param>
        /// <param name="doitAfficher">True si l'on doit afficher les relations</param>
        /// <returns>True s'il en existe</returns>
        public bool EssaieExiste(int usagerId, bool doitAfficher)
        {
            using (var context = new TP2BD21282433Entities())
            {
                var impression = context.Impressions
                                    .Where(c => c.UsagerId == usagerId).ToList();
                ;
                if (impression.Count > 0)
                {
                    if (doitAfficher)
                    {
                        EssaieImpression(impression);
                    }
                    return true;
                }
                else return false;
            }
        }

        /// <summary>
        /// Permet de vérifier s'il existe des essaies pour une impression.
        /// </summary>
        /// <param name="impression">Une liste d'impressions</param>
        public void EssaieImpression(List<Impression> impression)
        {
            List<Essaie> listeEssaie = new List<Essaie>();
            bool afficher = false;
            foreach (Impression imp in impression)
            {
                using (var context = new TP2BD21282433Entities())
                {
                    var essaie = context.Essaies
                                        .Where(c => c.ImpressionId == imp.ID).ToList();
                    ;
                    if (essaie.Count > 0)
                    {
                        listeEssaie = essaie;
                        afficher = true;
                    }
                }
            }
            if (afficher)
            {
                AfficherEssaies(listeEssaie);
            }
        }

        /// <summary>
        /// Permet d'afficher des essaies d'une liste.
        /// </summary>
        /// <param name="listeEssaie">La liste contenant les essaies</param>
        private void AfficherEssaies(List<Essaie> listeEssaie)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n Les Essaies suivants seront supprimés:");
            foreach (Essaie ess in listeEssaie)
            {
                Console.WriteLine("\n Essaie ID: {0} | Commentaire: {1}",
                        ess.ID,
                        ess.Commentaire);
            }
            Console.ResetColor();
        }
    }
}
