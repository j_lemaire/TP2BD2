﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2.CleeEtrangeres
{
    class FilamentClee
    {

        /// <summary>
        /// Permet de savoir si un filament est relié à un usager.
        /// </summary>
        /// <param name="usagerId">L'id de l'usager</param>
        /// <param name="doitAfficher">True s'il faut afficher les filaments</param>
        /// <returns></returns>
        public bool Existe(int usagerId, bool doitAfficher)
        {
            using (var context = new TP2BD21282433Entities())
            {
                var filament = context.Filaments
                                    .Where(c => c.ProprietaireId == usagerId).ToList();
                ;
                if (filament.Count > 0)
                {
                    if (doitAfficher)
                    {
                        Afficher(filament);
                    }
                    return true;
                }
                else return false;
            }
        }

        /// <summary>
        /// Permet d'afficher les Filaments.
        /// </summary>
        /// <param name="filament">Une liste de Filament</param>
        public void Afficher(List<Filament> filament)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\n Les filaments suivants seront supprimés:");
            foreach (Filament fil in filament)
            {
                Console.WriteLine("\n Filament ID: {0} | Couleur: {1}",
                        fil.ID,
                        fil.Couleur.Trim());
            }
            Console.ResetColor();
        }

        /// <summary>
        /// Vérifie si un essaieFilament existe pour un usager.
        /// </summary>
        /// <param name="usagerId">L'id de l'usager</param>
        /// <param name="doitAfficher">True si l'on doit afficher les relations</param>
        /// <returns></returns>
        public bool EssaieFilamentExiste(int usagerId, bool doitAfficher)
        {
            using (var context = new TP2BD21282433Entities())
            {
                var filament = context.Filaments
                                    .Where(c => c.ProprietaireId == usagerId).ToList();
                ;
                if (filament.Count > 0)
                {
                    if (doitAfficher)
                    {
                        EssaieFilament(filament);
                    }
                    return true;
                }
                else return false;
            }
        }

        /// <summary>
        /// Permet d'afficher les EssaieFilaments reliés à un usager.
        /// </summary>
        /// <param name="filament">Les filaments de l'usager</param>
        public void EssaieFilament(List<Filament> filament)
        {
            List<EssaieFilament> listeEssaieFil = new List<EssaieFilament>();
            bool afficher = false;
            foreach (Filament fil in filament)
            {
                using (var context = new TP2BD21282433Entities())
                {
                    var essaieFilament = context.EssaieFilaments
                                        .Where(c => c.FilamentId == fil.ID).ToList();
                    ;
                    if (essaieFilament.Count > 0)
                    {
                        listeEssaieFil = essaieFilament;
                        afficher = true;
                    }
                }
            }
            if (afficher)
            {
                AfficherEssaies(listeEssaieFil);
            }
        }

        /// <summary>
        /// Permet d'afficher les EssaiesFilaments.
        /// </summary>
        /// <param name="listeEssaieFil">Liste d'essaieFilaments</param>
        private void AfficherEssaies(List<EssaieFilament> listeEssaieFil)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\n Les EssaieFilaments suivants seront supprimés:");
            foreach (EssaieFilament ef in listeEssaieFil)
            {
                Console.WriteLine("\n Filament ID: {0} | Essaie ID: {1}",
                        ef.FilamentId,
                        ef.EssaieId);
            }
            Console.ResetColor();
        }
    }
}
