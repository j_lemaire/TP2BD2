﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe permettant d'accèder aux modèles d'imprimantes.
    /// </summary>
    class ImprimanteModele
    {

        /// <summary>
        /// Constructeur de ImprimanteModele
        /// </summary>
        public ImprimanteModele()
        {

        }

        /// <summary>
        /// Obtient les Id des usagers dans la table Usager.
        /// </summary>
        /// <returns>Une liste avec les Id des usagers</returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var modeleImprimantes = context.ModeleImprimantes;
                foreach (var ModeleImprimantes in modeleImprimantes)
                {
                    Console.WriteLine("\nID: {0} | Description: {1} ",
                                    ModeleImprimantes.ID,
                                    ModeleImprimantes.Description.Trim()
                                    );

                    listeId.Add(ModeleImprimantes.ID);
                }
            }
            return listeId;
        }
    }
}
