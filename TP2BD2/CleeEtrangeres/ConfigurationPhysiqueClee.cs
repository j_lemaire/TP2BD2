﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe permettant d'accèder aux configurations physiques.
    /// </summary>
    class ConfigurationPhysique
    {

        /// <summary>
        /// Constructeur de ConfigurationPhysique.
        /// </summary>
        public ConfigurationPhysique()
        {
        }

        /// <summary>
        /// Obtient les Id des configurations physiques dans la table ConfigurationPhysique.
        /// </summary>
        /// <returns>Une liste avec les Id des configurations physiques de ConfigurationPhysique</returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var configPhysiques = context.ConfigPhysiques;
                foreach (var ConfigPhysiques in configPhysiques)
                {
                    Console.WriteLine("\nID: {0} | Description: {1}",
                                    ConfigPhysiques.ID,
                                    ConfigPhysiques.Description.Trim()
                                    );

                    listeId.Add(ConfigPhysiques.ID);
                }
            }
            return listeId;
        }

        /// <summary>
        /// Permet de savoir si une configuration physique est relié à une imprimante.
        /// </summary>
        /// <param name="imprimanteId">L'id de l'imprimante</param>
        /// <param name="doitAfficher">True s'il faut afficher les configurations</param>
        /// <returns></returns>
        public bool Existe(int imprimanteId, bool doitAfficher)
        {
            using (var context = new TP2BD21282433Entities())
            {
                var config = context.ConfigPhysiques
                                    .Where(c => c.ImprimanteId == imprimanteId).ToList();
                ;
                if (config.Count > 0)
                {
                    if (doitAfficher)
                    {
                        Afficher(config);
                    }
                    return true;
                }
                else return false;
            }
        }

        /// <summary>
        /// Permet d'afficher les Configurations Physiques.
        /// </summary>
        /// <param name="configuration">Une liste de Configurations Physiques</param>
        public void Afficher(List<ConfigPhysique> configuration)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\n Les ConfigurationPhysiques suivantes seront supprimées:");
            foreach (ConfigPhysique cp in configuration)
            {
                Console.WriteLine("\n Configuration ID: {0} | Description: {1}",
                        cp.ID,
                        cp.Description.Trim());
            }
            Console.ResetColor();
        }
    }
}
