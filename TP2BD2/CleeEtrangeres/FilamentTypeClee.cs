﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe permettant d'accèder aux types de filaments.
    /// </summary>
    class FilamentType
    {
        /// <summary>
        /// Constructeur de FilamentType
        /// </summary>
        public FilamentType()
        {

        }

        /// <summary>
        /// Obtient les Id des types de filaments dans la table TypeFilament.
        /// </summary>
        /// <returns>Une liste avec les Id des types de filaments</returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var typeFilaments = context.TypeFilaments;
                foreach (var TypeFilaments in typeFilaments)
                {
                    Console.WriteLine("\nID: {0} | Matériel: {1} | Diamètre: {2}",
                                    TypeFilaments.ID,
                                    TypeFilaments.Materiel.Trim(),
                                    TypeFilaments.Diametre
                                    );

                    listeId.Add(TypeFilaments.ID);
                }
            }
            return listeId;
        }
    }
}
