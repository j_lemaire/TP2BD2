﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe permettant d'accèder aux configurations logiques.
    /// </summary>
    class ConfigurationLogique
    {

        /// <summary>
        /// Constructeur de ConfigurationLogique
        /// </summary>
        public ConfigurationLogique()
        {

        }

        /// <summary>
        /// Obtient les Id des configurations logiques dans la table ConfigLogique.
        /// </summary>
        /// <returns>Une liste avec les Id des configuration logiques de ConfigLogique</returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var configLogiques = context.ConfigLogiques;
                foreach (var ConfigLogiques in configLogiques)
                {
                    Console.WriteLine("\nID: {0} | Description: {1}",
                                    ConfigLogiques.ID,
                                    ConfigLogiques.Description.Trim()
                                    );

                    listeId.Add(ConfigLogiques.ID);
                }
            }
            return listeId;
        }

        /// <summary>
        /// Permet de savoir si une configuration logique est relié à une imprimante.
        /// </summary>
        /// <param name="imprimanteId">L'id de l'imprimante</param>
        /// <param name="doitAfficher">True s'il faut afficher les configurations</param>
        /// <returns></returns>
        public bool Existe(int imprimanteId, bool doitAfficher)
        {
            using (var context = new TP2BD21282433Entities())
            {
                var config = context.ConfigLogiques
                                    .Where(c => c.ImprimanteId == imprimanteId).ToList();
                ;
                if (config.Count > 0)
                {
                    if (doitAfficher)
                    {
                        Afficher(config);
                    }
                    return true;
                }
                else return false;
            }
        }

        /// <summary>
        /// Permet d'afficher les Configurations Logiques.
        /// </summary>
        /// <param name="configuration">Une liste de Configurations Logiques</param>
        public void Afficher(List<ConfigLogique> configuration)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\n Les ConfigurationLogiques suivantes seront supprimées:");
            foreach (ConfigLogique cl in configuration)
            {
                Console.WriteLine("\n Configuration ID: {0} | Description: {1}",
                        cl.ID,
                        cl.Description.Trim());
            }
            Console.ResetColor();
        }

        /// <summary>
        /// Permet de vérifier si un essaie existe relié à un imprimante.
        /// </summary>
        /// <param name="imprimanteId">L'id de l'imprimante</param>
        /// <param name="doitAfficher">True si l'on doit afficher les relations</param>
        /// <returns></returns>
        public bool EssaieExiste(int imprimanteId, bool doitAfficher)
        {
            using (var context = new TP2BD21282433Entities())
            {
                var configLogique = context.ConfigLogiques
                                    .Where(c => c.ImprimanteId == imprimanteId).ToList();
                ;
                if (configLogique.Count > 0)
                {
                    if (doitAfficher)
                    {
                        EssaieConfiguration(configLogique);
                    }
                    return true;
                }
                else return false;
            }
        }

        /// <summary>
        /// Permet d'afficher les essaies des configurations.
        /// </summary>
        /// <param name="configLogique">la configuration logique</param>
        public void EssaieConfiguration(List<ConfigLogique> configLogique)
        {
            List<Essaie> listeEssaie = new List<Essaie>();
            bool afficher = false;
            foreach (ConfigLogique conf in configLogique)
            {
                using (var context = new TP2BD21282433Entities())
                {
                    var essaie = context.Essaies
                                        .Where(c => c.ImpressionId == conf.ID).ToList();
                    ;
                    if (essaie.Count > 0)
                    {
                        listeEssaie = essaie;
                        afficher = true;
                    }
                }
            }
            if (afficher)
            {
                AfficherEssaies(listeEssaie);
            }
        }

        /// <summary>
        /// Affiche les essaie de la liste d'essaies.
        /// </summary>
        /// <param name="listeEssaie">Une liste contenant des essaies</param>
        private void AfficherEssaies(List<Essaie> listeEssaie)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\n Les Essaies suivants seront supprimés:");
            foreach (Essaie ess in listeEssaie)
            {
                Console.WriteLine("\n Essaie ID: {0} | Commentaire: {1}",
                        ess.ID,
                        ess.Commentaire);
            }
            Console.ResetColor();
        }
    }
}
