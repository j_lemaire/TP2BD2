﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe permettant d'accèder aux fabricants des filaments.
    /// </summary>
    class FabricantFilament
    {

        /// <summary>
        /// Constructeur de FabricantFilament
        /// </summary>
        public FabricantFilament()
        {

        }

        /// <summary>
        /// Obtient les Id des types de filaments dans la table TypeFilament.
        /// </summary>
        /// <returns>Une liste avec les Id des types de filaments</returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var fabricants = context.Fabricants;
                foreach (var Fabricants in fabricants)
                {
                    Console.WriteLine("\nID: {0} | Nom: {1}",
                                    Fabricants.ID,
                                    Fabricants.Nom.Trim()
                                    );

                    listeId.Add(Fabricants.ID);
                }
            }
            return listeId;
        }
    }
}
