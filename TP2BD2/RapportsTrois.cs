﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe permettant de générer un rapport en utilisant les deux syntaxes de projection.
    /// </summary>
    class RapportsTrois
    {
        protected Vue _menu;
        protected GestionUsager _usager;
        protected Constantes _constantes;

        /// <summary>
        /// Constructeur de RapportsTrois
        /// </summary>
        public RapportsTrois()
        {
            _menu = new Vue();
            _usager = new GestionUsager();
            _constantes = new Constantes();
        }

        /// <summary>
        /// Menu principal du rapport trois
        /// </summary>
        public void MenuPrincipal()
        {
            List<String> optionsMenu = new List<string>
            {
                "1 - Utiliser la syntaxe par méthodes",
                "2 - Utiliser la syntaxe par requêtes",
                "0 - Retour"
            };

            int choix;
            int id;
            string question = _constantes.choix;
            string usagerId = _constantes.usagerId;
            do
            {
                _menu.AfficherMaListe(optionsMenu);
                choix = _menu.ChoisirOption(new List<int> { 0, 1, 2}, question);
                List<int> listeIdUsager = new List<int>();
                listeIdUsager = _usager.ObtenirListe();

                if (_menu.ListeVide(listeIdUsager))
                {
                    Console.WriteLine("Il n'y à pas d'usagers !");
                    Console.WriteLine("Pressez sur entré !");
                    Console.ReadLine();
                    choix = 0;
                }
                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            id = _menu.ChoisirOption(listeIdUsager, usagerId);
                            RapportImpressionsMethodes(id);
                            break;
                        case 2:
                            id = _menu.ChoisirOption(listeIdUsager, usagerId);
                            RapportImpressionsRequetes(id);
                            break;
                        default:
                            break;
                    }
                }
            } while (choix != 0);
        }

        /// Question F1 A
        /// <summary>
        /// Permet de faire un rapport des impressions par usagers en utilisant la syntaxe par méthodes.
        /// </summary>
        /// <param name="id">Le id de l'usager</param>
        private void RapportImpressionsMethodes(int id)
        {
            Console.Clear();
            using (var context = new TP2BD21282433Entities())
            {
                var usagers = context.Usagers
                            .Where(c => c.ID == id)
                            .Select(c => new { c.Prenom, c.Nom, c.Impressions.Count });


                foreach (var usager in usagers)
                {
                    Console.WriteLine("\n Nom: {0} | Prenom: {1} | Nombre Impressions: {2}",
                            usager.Prenom.Trim(),
                            usager.Nom.Trim(),
                            usager.Count);
                }

            }
            Console.Write("\n Pressez sur entrée -->");
            Console.ReadLine();
        }

        /// Question F1 B
        /// <summary>
        /// Permet de faire un rapport des impressions par usagers en utilisant la syntaxe par requêtes.
        /// </summary>
        /// <param name="id">L'id de l'usager</param>
        private void RapportImpressionsRequetes(int id)
        {
            Console.Clear();

            using (var context = new TP2BD21282433Entities())
            {
                var usagers = from c in context.Usagers
                              where c.ID == id
                              select new { c.Prenom, c.Nom, c.Impressions.Count };

                foreach (var usager in usagers)
                {
                    Console.WriteLine("\n Nom: {0} | Prenom: {1} | Nombre Impressions: {2}",
                            usager.Prenom.Trim(),
                            usager.Nom.Trim(),
                            usager.Count);
                }

            }
            Console.Write("\n Pressez sur entrée -->");
            Console.ReadLine();
        }
    }
}
