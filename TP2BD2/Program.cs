﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP2BD2.Controleur;

/// <summary>
/// Nom: Joël Lemaire-Younoussa
/// </summary>
namespace TP2BD2
{   
    class Program
    {
        public static void Main()
        {
            List<String> Menu = new List<string>
            {
                "\n1 - CRUD de base",
                "2 - Essaie",
                "3 - CRUD configuration",
                "4 - Rapports",
                "5 - Rapports 2",
                "6 - Rapports 3",
                "0 - quitter"
            };
            Constantes _constantes = new Constantes();
            ChoisirCrud _crud = new ChoisirCrud();
            RapportsTrois _rapportTrois = new RapportsTrois();
            EssaieCtrl _essai = new EssaieCtrl();
            CrudConfiguration _configuration = new CrudConfiguration();
            RapportsUn _rapports = new RapportsUn();
            RapportsDeux _rapportDeux = new RapportsDeux();
            Vue menuPrincipal = new Vue();
            int choix;
            string question = _constantes.choix;
            do
            {
                menuPrincipal.AfficherMaListe(Menu);
                choix = menuPrincipal.ChoisirOption(new List<int> { 0, 1, 2, 3, 4, 5, 6 }, question);
                if (choix != 0)
                {
                    TP2BD21282433Entities context = new TP2BD21282433Entities();
                    using (context)
                    {
                        switch (choix)
                        {
                            case (1):
                                _crud.MenuPrincipal();
                                break;
                            case (2):
                                _essai.MenuPrincipal();
                                break;
                            case (3):
                                _configuration.MenuPrincipal();
                                break;
                            case (4):
                                _rapports.MenuPrincipal();
                                break;
                            case (5):
                                _rapportDeux.MenuPrincipal();
                                break;
                            case (6):
                                _rapportTrois.MenuPrincipal();
                                break;
                            default:
                                break;
                        }
                    }
                }
            } while (choix != 0);
        }
    }
}
