﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    class Constantes
    {
        public string choix;
        public string usagerId;
        public string filamentId;
        public string imprimanteDesc;
        public string prix;
        public string modeleId;
        public string nom;
        public string prenom;
        public string motDePasse;
        public string imprimanteId;
        public string couleur;
        public string longueurInitiale;
        public string date;
        public string proprietaire;
        public string typeFilament;
        public string fabricant;
        public string poidInitial;
        public string heure;
        public string dureeReelle;
        public string dureeEstimee;
        public string commentaire;
        public string configLogiqueId;
        public string configPhysiqueId;
        public string impressionId;
        public string pourcentage;
        public string quantiteFilament;
        public string essaieId;
        public string nouvelleQte;
        public string configUtilise;
        public string creationId;

        public Constantes()
        {
            choix = "\nEntrez votre choix: ";
            usagerId = "\nEntrez l'id de l'usager: ";
            filamentId = "\nEntrez l'id du filament: ";
            imprimanteDesc = "\nEntrez la description de l'imprimante: ";
            prix = "\nEntrez le prix: ";
            modeleId = "\nEntrez le id du modèle: ";
            nom = "\nEntrez le nom: ";
            prenom = "\nEntrez le prenom: ";
            motDePasse = "\nEntrez le mot de passe: ";
            imprimanteId = "\nEntrez l'id de l'imprimante: ";
            couleur = "\nEntrez le nombre correspondant à la couleur: ";
            longueurInitiale = "\nEntrez la longueur initiale: ";
            date = "\nEntrez une date (année/mois/jour): ";
            proprietaire = "\nEntrez l'id du propriétaire: ";
            typeFilament = "\nEntrez l'id du type de filament: ";
            fabricant = "\nEntrez l'id du fabricant: ";
            poidInitial = "\nEntrez le poid initial: ";
            heure = "\nEntrez l'heure de depart(00:00:00): ";
            dureeReelle = "\nEntrez la durée réelle: ";
            dureeEstimee = "\nEntrez la durée estimée: ";
            commentaire = "\nEntrez un commentaire: ";
            configLogiqueId = "\nEntrez l'id de la configuration logique: ";
            configPhysiqueId = "\nEntrez l'id de la configuration physique: ";
            impressionId = "\nEntrez l'id de l'impression: ";
            pourcentage = "\nEntrez le pourcentage de réussite: ";
            quantiteFilament = "\nEntrez la quantité de filament utilisé par l'essaie: ";
            essaieId = "\nEntrez l'id de l'essaie: ";
            nouvelleQte = "\nEntrez la nouvelle quantité de filament: ";
            configUtilise = "\nLa configuration est associé à un ou plusieurs essaies: ";
            creationId = "\nEntrez l'id de la création à afficher: ";
        }
    }
}
