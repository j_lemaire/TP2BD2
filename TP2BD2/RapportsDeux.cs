﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// Question E - Menu
    /// <summary>
    /// Classe permettant de faire du lazy loading, eager loading et de la projection.
    /// </summary>
    class RapportsDeux
    {
        protected Vue _menu;
        protected Constantes _constantes;
        protected GestionUsager _usager;

        /// <summary>
        /// Constructeur de RapportsDeux
        /// </summary>
        public RapportsDeux()
        {
            _menu = new Vue();
            _usager = new GestionUsager();
            _constantes = new Constantes();
        }

        /// <summary>
        /// Menu principal du rapport 2
        /// </summary>
        public void MenuPrincipal()
        {
            List<String> optionsMenu = new List<string>
            {
                "1 - Utiliser le Lazy loading",
                "2 - Utiliser le Eager loading",
                "3 - Utiliser une Projection",
                "0 - Retour"
            };

            int choix;
            string question = _constantes.choix;
            do
            {
                _menu.AfficherMaListe(optionsMenu);
                choix = _menu.ChoisirOption(new List<int> { 0, 1, 2, 3 }, question);
                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            RequeteLazyLoading();
                            break;
                        case 2:
                            RequeteEagerLoading();
                            break;

                        case 3:
                            RequeteProjection();
                            break;
                        default:
                            break;
                    }
                }
            } while (choix != 0);
        }

        /// Question E1 A
        /// <summary>
        /// Permet de faire le rapport des usagers et de leurs impressions en utilisant le lazyLoading.
        /// </summary>
        private void RequeteLazyLoading()
        {
            Console.Clear();
            using (var context = new TP2BD21282433Entities())
            {
                var usagers = context.Usagers;

                foreach (var usager in usagers)
                {
                    Console.WriteLine("\n---------------------------------------------------------------");
                    Console.WriteLine(
                        "\nUsager ID: {0}" +
                        "\nPrenom: {1}" +
                        "\nNom: {2}" +
                        "\nNombre d'impressions: {3}",
                        usager.ID,
                        usager.Prenom.Trim(),
                        usager.Nom.Trim(),
                        usager.Impressions.Count);

                    foreach (var impression in usager.Impressions)
                    {
                        Console.WriteLine(
                            "\nCréation ID: {0} " +
                            "\nNom: {1}" +
                            "\nDescription: {2}" +
                            "\nURL: {3}",
                            impression.Creation.ID,
                            impression.Creation.Nom.Trim(),
                            impression.Creation.Description.Trim(),
                            impression.Creation.URLSource.Trim()
                            );
                    }
                }
            }
            Console.Write("\n\nPressez sur Entrée --->");
            Console.Read();
        }

        /// Question E1 B
        /// <summary>
        /// Permet de faire le rapport des usagers et leurs impressions en utilisant le eagerLoading.
        /// </summary>
        private void RequeteEagerLoading()
        {
            Console.Clear();
            using (var context = new TP2BD21282433Entities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                var usagers = context.Usagers.Include("Impressions");

                foreach (var usager in usagers)
                {
                    Console.WriteLine("\n---------------------------------------------------------------");
                    Console.WriteLine(
                        "\nUsager ID: {0}" +
                        "\nPrenom: {1}" +
                        "\nNom: {2}" +
                        "\nNombre d'impressions: {3}",
                        usager.ID,
                        usager.Prenom.Trim(),
                        usager.Nom.Trim(),
                        usager.Impressions.Count);

                    var impressions = context.Impressions.Include("Creation")
                    .Where(i => i.UsagerId == usager.ID);

                    foreach (var impression in impressions)
                    {
                        Console.WriteLine(
                            "\nCréation ID: {0} " +
                            "\nNom: {1}" +
                            "\nDescription: {2}" +
                            "\nURL: {3}",
                            impression.Creation.ID,
                            impression.Creation.Nom.Trim(),
                            impression.Creation.Description.Trim(),
                            impression.Creation.URLSource.Trim()
                            );
                    }
                }
            }
            Console.Write("\n\nPressez sur Entrée --->");
            Console.Read();
        }

        /// Question E1 C
        /// <summary>
        /// Permet de faire le rapport des usagers et de leurs impressions en utilisant une projection.
        /// </summary>
        private void RequeteProjection()
        {
            Console.Clear();
            using (var context = new TP2BD21282433Entities())
            {
                var usagers = from c in context.Usagers
                               select new { c.Impressions, c.Prenom, c.Nom, c.ID};

                foreach (var usager in usagers)
                {
                    Console.WriteLine("\n---------------------------------------------------------------");
                    Console.WriteLine(
                        "\nUsager ID: {0}" +
                        "\nPrenom: {1}" +
                        "\nNom: {2}" +
                        "\nNombre d'impressions: {3}",
                        usager.ID,
                        usager.Prenom.Trim(),
                        usager.Nom.Trim(),
                        usager.Impressions.Count);

                    var impressions = context.Impressions
                        .Where(c => c.UsagerId == usager.ID)
                        .Select(c => new { c.Creation.ID, c.Creation.Nom, c.Creation.Description, c.Creation.URLSource });

                    foreach (var impression in impressions)
                    {
                        Console.WriteLine(
                            "\nCréation ID: {0} " +
                            "\nNom: {1}" +
                            "\nDescription: {2}" +
                            "\nURL: {3}",
                            impression.ID,
                            impression.Nom.Trim(),
                            impression.Description.Trim(),
                            impression.URLSource.Trim()
                            );
                    }
                }
            }
            Console.Write("\n\nPressez sur Entrée --->");
            Console.Read();
        }
    }
}
