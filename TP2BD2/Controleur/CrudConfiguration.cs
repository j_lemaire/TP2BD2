﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP2BD2.Gestion;

namespace TP2BD2.Controleur
{
    /// Question C1 - Menu
    /// <summary>
    /// Classe de contrôle d'une configuration
    /// </summary>
    class CrudConfiguration
    {
        protected GestionConfiguration _configuration;
        protected Constantes _constantes;

        /// <summary>
        /// Constructeur de CrudConfiguration
        /// </summary>
        public CrudConfiguration()
        {
            _configuration = new GestionConfiguration();
            _constantes = new Constantes();
        }

        public void MenuPrincipal()
        {
            List<String> optionsMenu = new List<string>
            {
                "1 - Effacer une configuration",
                "0 - Retour"
            };

            Vue menu = new Vue();
            int choix;
            string question = _constantes.choix;
            do
            {
                menu.AfficherMaListe(optionsMenu);
                choix = menu.ChoisirOption(new List<int> { 0, 1}, question);
                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            _configuration.ChoisirEffacer();
                            break;
                        default:
                            break;
                    }
                }
            } while (choix != 0);
        }
    }
}
