﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe contrôle de filament.
    /// </summary>
    class CrudFilament
    {
        protected Vue _menu;
        protected GestionFilament _filament;
        protected Constantes _constantes;
        protected GestionEssaieFilament _essaieFilament;
        
        /// <summary>
        ///  Constructeur de CrudFilament
        /// </summary>
        public CrudFilament()
        {
            _menu = new Vue();
            _filament = new GestionFilament();
            _constantes = new Constantes();
            _essaieFilament = new GestionEssaieFilament();
        }

        /// <summary>
        /// Menu du crud pour le filament
        /// </summary>
        public void MenuPrincipal()
        {
            List<String> optionsMenu = new List<string>
            {
                "1 - Lire table filament",
                "2 - Créer un filament",
                "3 - Modifier un filament",
                "4 - Supprimer un filament",
                "0 - Retour"
            };

            int choix;
            string question = _constantes.choix;
            do
            {
                _menu.AfficherMaListe(optionsMenu);
                choix = _menu.ChoisirOption(new List<int> { 0, 1, 2, 3, 4 }, question);
                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            _filament.Lire();
                            break;
                        case 2:
                            _filament.Creer();
                            break;
                        case 3:
                            _filament.Modifier();
                            break;
                        case 4:
                            _essaieFilament.ChoisirEffacer();
                            break;
                        default:
                            break;
                    }
                }
            } while (choix != 0);
        }
    }
}
