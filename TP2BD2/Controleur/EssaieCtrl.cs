﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe contrôle d'essaie.
    /// </summary>
    class EssaieCtrl
    {
        protected Vue _menu;
        protected GestionEssai _essai;
        protected Constantes _constantes;

        /// <summary>
        /// Constructeur de Essai.
        /// </summary>
        public EssaieCtrl()
        {
            _menu = new Vue();
            _essai = new GestionEssai();
            _constantes = new Constantes();
        }

        /// <summary>
        /// Menu principal pour créer et modifier des essais.
        /// </summary>
        public void MenuPrincipal()
        {
            List<String> optionsMenu = new List<string>
            {
                "1 - Créer un essaie",
                "2 - Modifier la quantité de filament d'un essaie",
                "3 - Ajouter un filament à un essaie",
                "0 - Retour"
            };

            int choix;
            string question = _constantes.choix;
            do
            {
                _menu.AfficherMaListe(optionsMenu);
                choix = _menu.ChoisirOption(new List<int> { 0, 1, 2, 3}, question);
                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            _essai.Creer();
                            break;
                        case 2:
                            _essai.ModifierQte();
                            break;
                        case 3:
                            _essai.AjouterFilament();
                            break;
                        default:
                            break;
                    }
                }
            } while (choix != 0);
        }
    }
}
