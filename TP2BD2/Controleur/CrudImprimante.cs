﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe contrôle d'imprimante.
    /// </summary>
    class CrudImprimante
    {
        protected GestionImprimante _imprimante;
        protected Constantes _constantes;
        protected Vue _menu;

        /// <summary>
        /// Constructeur de GestionImprimante
        /// </summary>
        public CrudImprimante()
        {
            _imprimante = new GestionImprimante();
            _constantes = new Constantes();
            _menu = new Vue();
        }

        public void MenuPrincipal()
        {
            List<String> optionsMenu = new List<string>
            {
                "1 - Lire table imprimante",
                "2 - Créer une imprimante",
                "3 - Modifier une imprimante",
                "4 - Supprimer une imprimante",
                "0 - Retour"
            };

            int choix;
            string question = _constantes.choix;
            do
            {
                _menu.AfficherMaListe(optionsMenu);
                choix = _menu.ChoisirOption(new List<int> { 0, 1, 2, 3, 4 }, question);
                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            _imprimante.Lire();
                            break;
                        case 2:
                            _imprimante.Creer();
                            break;
                        case 3:
                            _imprimante.Modifier();
                            break;
                        case 4:
                            _imprimante.ChoisirEffacer();
                            break;
                        default:
                            break;
                    }
                }
            } while (choix != 0);
        }
    }
}
