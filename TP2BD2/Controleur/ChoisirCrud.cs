﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe de contrôle pour choisir le crud approprié.
    /// </summary>
    class ChoisirCrud
    {
        protected Vue _menu;
        protected CrudUsager _crudUsager;
        protected CrudImprimante _crudImprimante;
        protected CrudFilament _crudFilament;
        protected Constantes _constantes;

        /// <summary>
        /// Constructeur de ChoisirCrud
        /// </summary>
        public ChoisirCrud()
        {
            _menu = new Vue();
            _crudUsager = new CrudUsager();
            _crudImprimante = new CrudImprimante();
            _crudFilament = new CrudFilament();
            _constantes = new Constantes();
        }

        /// <summary>
        /// Menu principal pour choisir entre le crud d'une imprimante, d'un usager ou d'un filament
        /// </summary>
        public void MenuPrincipal()
        {
            List<String> optionsMenu = new List<string>
            {
                "1 - CRUD d'une imprimante ",
                "2 - CRUD d'un usager",
                "3 - CRUD d'un filament",
                "0 - Retour"
            };


            int choix;
            string question = _constantes.choix;
            do
            {
                _menu.AfficherMaListe(optionsMenu);
                choix = _menu.ChoisirOption(new List<int> { 0, 1, 2, 3 }, question);
                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            _crudImprimante.MenuPrincipal();
                            break;
                        case 2:
                            _crudUsager.MenuPrincipal();
                            break;
                        case 3:
                            _crudFilament.MenuPrincipal();
                            break;
                        default:
                            break;
                    }
                }
            } while (choix != 0);
        }
    }
}
