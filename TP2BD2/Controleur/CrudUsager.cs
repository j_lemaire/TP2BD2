﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe contrôle d'usager.
    /// </summary>
    class CrudUsager
    {
        protected GestionUsager _usager;
        protected Constantes _constantes;

        /// <summary>
        /// Constructeur de CrudUsager
        /// </summary>
        public CrudUsager()
        {
            _usager = new GestionUsager();
            _constantes = new Constantes();
        }

        public void MenuPrincipal()
        {
            List<String> optionsMenu = new List<string>
            {
                "1 - Lire table usager",
                "2 - Créer un usager",
                "3 - Modifier un usager",
                "4 - Supprimer un usager",
                "0 - Retour"
            };

            Vue menu = new Vue();
            int choix;
            string question = _constantes.choix;
            do
            {
                menu.AfficherMaListe(optionsMenu);
                choix = menu.ChoisirOption(new List<int> { 0, 1, 2, 3, 4 }, question);
                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            _usager.Lire();
                            break;
                        case 2:
                            _usager.Creer();
                            break;
                        case 3:
                            _usager.Modifier();
                            break;
                        case 4:
                            _usager.ChoisirEffacer();
                            break;
                        default:
                            break;
                    }
                }
            } while (choix != 0);
        }
    }
}
