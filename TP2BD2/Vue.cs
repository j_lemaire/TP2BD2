﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    class Vue
    {
        /// <summary>
        /// Permet d'afficher une liste avec des options.
        /// </summary>
        /// <param name="lignesMenu">Une liste avec des lignes qui sont les options du menu</param>
        public void AfficherMaListe(List<string> lignesMenu)
        {
            Console.Clear();
            foreach (string ligne in lignesMenu)
            {
                Console.WriteLine(ligne);
            }
        }

        /// <summary>
        /// Permet d'afficher les couleurs d'une liste.
        /// </summary>
        /// <param name="couleurs">La liste de couleurs</param>
        public void AfficherCouleurs(List<string> couleurs)
        {
            Console.Clear();    
            for (int i = 0; i < couleurs.Count; i++)
            {
                string value = couleurs[i];
                Console.WriteLine($"{i} - {value}");
            }
        }

        /// <summary>
        /// Permet de vérifier si le string est vide ou null.
        /// </summary>
        /// <param name="leString">Un string à vérifier</param>
        /// <returns>Vrai ou faux</returns>
        public bool StringEstNull(string leString)
        {
            if (string.IsNullOrWhiteSpace(leString) || string.IsNullOrEmpty(leString))
            {
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Permet de choisir une option valide dans la liste de choix.
        /// </summary>
        /// <param name="choixValides">La liste de choix valides</param>
        /// <param name="question">La question à poser</param>
        /// <returns></returns>
        public int ChoisirOption(List<int> choixValides, string question)
        {
            int choix;
            do
            {
                Console.Write(question);
                string c_choix = Console.ReadLine();
                if (!Int32.TryParse(c_choix, out choix))
                { choix = choixValides.Min() - 1; }
            } while (!choixValides.Contains(choix));

            return choix;
        }

        /// <summary>
        /// Input d'une chaine de caractères.
        /// </summary>
        /// <param name="question">la question à poser</param>
        /// <param name="defaut">la valeur par défaut si l'usager presse Return</param>
        /// <returns>la chaine de caractère entrée</returns>
        public string InputStringNonNull(string question)
        {
            string reponse = null;
            while (StringEstNull(reponse))
            {
                Console.Write(question);
                reponse = Console.ReadLine();
            }
            return reponse;
        }

        /// <summary>
        /// Permet de vérifier que le input est un int ou null.
        /// </summary>
        /// <param name="question">La question à poser</param>
        /// <param name="defaut">la valeur par defaut</param>
        /// <returns></returns>
        public int InputInt(string question, int? defaut = null)
        {
            string reponse;
            int reponseInt;
            do
            {
                Console.Write(question);
                if (defaut != null)
                {
                    Console.Write(" ( " + defaut + " )");
                }
                reponse = Console.ReadLine();
                if (reponse == "" && defaut != null)
                {
                    reponse = defaut.ToString();
                }
            } while (!Int32.TryParse(reponse, out reponseInt));
            return reponseInt;

        }

        /// <summary>
        /// Permet de prendre un decimal en input.
        /// </summary>
        /// <param name="question">La question à poser</param>
        /// <returns></returns>
        public decimal InputDecimal(string question)
        {
            string reponse;
            decimal reponseDecimal;
            do
            {
                Console.Write(question);
                reponse = Console.ReadLine();
                reponse = reponse.Replace(",", ".");
            } while (!decimal.TryParse(reponse, out reponseDecimal));
            return reponseDecimal;

        }

        /// <summary>
        /// Permet de prendre un float en input.
        /// </summary>
        /// <param name="question">La question à poser</param>
        /// <returns>Le nombre en float</returns>
        public float InputFloat(string question)
        {
            string reponse;
            float reponseFloat;
            do
            {
                Console.Write(question);
                reponse = Console.ReadLine();
                reponse = reponse.Replace(",", ".");
            } while (!float.TryParse(reponse, out reponseFloat));
            return reponseFloat;
        }

        /// <summary>
        /// Permet de prendre une date en input.
        /// </summary>
        /// <param name="question">La question à poser</param>
        /// <returns>La date en format DateTime</returns>
        public DateTime InputDate(string question)
        {
            string reponse;
            DateTime reponseDate = DateTime.Now;
            bool bonneDate;
            do
            {
                Console.Write(question);
                reponse = Console.ReadLine();
                bonneDate = true;
                try
                {
                reponseDate = Convert.ToDateTime(reponse);
                }
                catch (Exception)
                {
                    bonneDate = false;
                }
            } while (!bonneDate);
            return reponseDate;
        }

        /// <summary>
        /// Permet de prendre un input d'un string null ou non.
        /// </summary>
        /// <param name="question">La question à poser</param>
        /// <param name="defaut">la valeur par defaut</param>
        /// <returns>le string</returns>
        public string InputString(string question, string defaut = null)
        {
            Console.Write(question);
            if (defaut != null)
            {
                Console.Write(" ( " + defaut + " )");
            }
            string reponse = Console.ReadLine();
            if (reponse == "" && defaut != null)
            {
                reponse = defaut;
            }

            return reponse;
        }

        /// <summary>
        /// Permet d'afficher une liste avec des choix.
        /// </summary>
        /// <param name="lesChoix">Une liste avec des choix</param>
        public void AfficherChoix(List<string> lesChoix)
        {
            Console.WriteLine("\n");
            foreach (string choix in lesChoix)
            {
                Console.WriteLine(choix);
            }
        }

        /// <summary>
        /// Permet de vérifier qu'une liste n'est pas vide.
        /// </summary>
        /// <param name="liste">La liste à vérifier</param>
        /// <returns>True si elle est vide</returns>
        public bool ListeVide(List<int> liste)
        {
            if (liste.Count < 1)
            {
                Console.Clear();
                Console.WriteLine("\nIl manque de l'information dans la base de données pour continuer...");
                Console.WriteLine("Pressez sur entré --->");
                Console.ReadLine();
                return true;
            }
            else return false;
        }
    }
}
