﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe de gestion d'essaie permettant de (lire, modifier, effacer et ajouter).
    /// </summary>
    class GestionEssai
    {
        protected Vue _vue;
        protected ImpressionClee _essaieImpression;
        protected ConfigurationLogique _configLogique;
        protected ConfigurationPhysique _configPhysique;
        protected GestionEssaieFilament _essaieFilament;
        protected Constantes _constantes;

       
        /// <summary>
        /// Constructeur de GestionEssai.
        /// </summary>
        public GestionEssai()
        {
            _vue = new Vue();
            _essaieImpression = new ImpressionClee();
            _configLogique = new ConfigurationLogique();
            _configPhysique = new ConfigurationPhysique();
            _essaieFilament = new GestionEssaieFilament();
            _constantes = new Constantes();
        }

        /// <summary>
        /// Obtient les Id des essaies dans la table Essaie.
        /// </summary>
        /// <returns>Une liste avec les Id des Essaies</returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var essaies = context.Essaies;
                foreach (var Essaies in essaies)
                {
                    Console.WriteLine("\nID: {0} | Commentaire: {1}",
                                    Essaies.ID,
                                    Essaies.Commentaire.Trim()
                                    );

                    listeId.Add(Essaies.ID);
                }
            }
            return listeId;
        }

        /// Question B1
        /// <summary>
        /// Permet de creer un Essaie complet.
        /// </summary>
        public void Creer()
        {
            Essaie nouvelEssaie = new Essaie();
            nouvelEssaie = Informations(nouvelEssaie);
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    var essaie = new Essaie
                    {
                        TempsDepart = nouvelEssaie.TempsDepart,
                        DureeReele = nouvelEssaie.DureeReele,
                        DureeEstime = nouvelEssaie.DureeEstime,
                        Commentaire = nouvelEssaie.Commentaire.Trim(),
                        ConfigLogiqueId = nouvelEssaie.ConfigLogiqueId,
                        ConfigPhysiqueId = nouvelEssaie.ConfigPhysiqueId,
                        ImpressionId = nouvelEssaie.ImpressionId,
                        PourcentageReussi = nouvelEssaie.PourcentageReussi,
                    };
                    context.Essaies.Add(essaie);
                    context.SaveChanges();
                    int essaieId = essaie.ID;
                    _essaieFilament.Creer(essaieId);
                }
            }
            catch { } 
        }

        /// <summary>
        /// Permet de setter les informations d'un nouvel Essaie avec l'input de l'usager.
        /// </summary>
        /// <param name="nouvelEssaie">Le nouvel Essaie à setter</param>
        /// <returns>Le nouvel Essaie</returns>
        public Essaie Informations(Essaie nouvelEssaie)
        {
            DateTime tempsDepart;
            int dureeReelle;
            int dureeEstimee;
            string commentaire;
            int configLogiqueId;
            int configPhysiqueId;
            int impressionId;
            int pourcentageReussi;

            tempsDepart = _vue.InputDate(_constantes.heure);
            dureeReelle = _vue.InputInt(_constantes.dureeReelle);
            dureeEstimee = _vue.InputInt(_constantes.dureeEstimee);
            commentaire = _vue.InputString(_constantes.commentaire);
            pourcentageReussi = _vue.InputInt(_constantes.pourcentage);
            List<int> listeImpressionId = new List<int>();
            List<int> listeConfigLogiqueId = new List<int>();
            List<int> listeConfigPhysiqueId = new List<int>();
            listeImpressionId = _essaieImpression.ObtenirListe();
            if (!_vue.ListeVide(listeImpressionId))
            {
                impressionId = _vue.ChoisirOption(listeImpressionId, _constantes.impressionId);
                listeConfigLogiqueId = _configLogique.ObtenirListe();
                if (!_vue.ListeVide(listeConfigLogiqueId))
                {
                    configLogiqueId = _vue.ChoisirOption(listeConfigLogiqueId, _constantes.configLogiqueId);
                    listeConfigPhysiqueId = _configPhysique.ObtenirListe();
                    if (!_vue.ListeVide(listeConfigPhysiqueId))
                    {
                        configPhysiqueId = _vue.ChoisirOption(listeConfigPhysiqueId, _constantes.configPhysiqueId);
                        nouvelEssaie.TempsDepart = tempsDepart;
                        nouvelEssaie.DureeReele = dureeReelle;
                        nouvelEssaie.DureeEstime = dureeEstimee;
                        nouvelEssaie.Commentaire = commentaire;
                        nouvelEssaie.PourcentageReussi = pourcentageReussi;
                        nouvelEssaie.ImpressionId = impressionId;
                        nouvelEssaie.ConfigLogiqueId = configLogiqueId;
                        nouvelEssaie.ConfigPhysiqueId = configPhysiqueId;
                    }
                }
            }
            return nouvelEssaie;
        }

        /// Question B2
        /// <summary>
        /// Permet de modifier la quantité de filament utilisé par un EssaieFilament.
        /// </summary>
        public void ModifierQte()
        {
            int essaieAModifier;
            int filamentEssaie;
            float quantite;
            List<int> listeEssaieId = new List<int>();
            listeEssaieId = ObtenirListe();
            if (!_vue.ListeVide(listeEssaieId))
            {
                essaieAModifier = _vue.ChoisirOption(listeEssaieId, _constantes.essaieId);
                List<int> liteFilamentId = new List<int>();
                liteFilamentId = _essaieFilament.ObtenirFilamentEssaie(essaieAModifier);
                if (!_vue.ListeVide(liteFilamentId))
                {
                    filamentEssaie = _vue.ChoisirOption(liteFilamentId, _constantes.filamentId);
                    quantite = _vue.InputFloat(_constantes.nouvelleQte);
                    try
                    {
                        using (var context = new TP2BD21282433Entities())
                        {
                            var essaieFilament = context.EssaieFilaments
                                                .Where(c => c.FilamentId == filamentEssaie && c.EssaieId == essaieAModifier).ToList();
                            ;

                            var Essaiefilament = essaieFilament[0];
                            Essaiefilament.QteUtilise = quantite;

                            context.SaveChanges();
                        }
                    }
                    catch { } 
                }
            }  
        }

        /// <summary>
        /// Permet d'ajouter plus d'un filament à un Essaie.
        /// </summary>
        public void AjouterFilament()
        {
            List<int> listeEssaieId = new List<int>();
            listeEssaieId = ObtenirListe();
            if (!_vue.ListeVide(listeEssaieId))
            {
                int essaieAAjouter = _vue.ChoisirOption(listeEssaieId, _constantes.essaieId);
                _essaieFilament.Creer(essaieAAjouter);
            }
        }
    }
}
