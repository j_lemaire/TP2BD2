﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe de gestion d'un Filament permettant de (Créer, lire, modifier et effacer).
    /// </summary>
    class GestionFilament
    {
        protected GestionUsager _usager;
        protected Vue _vue;
        protected FilamentType _typeFilament;
        protected FabricantFilament _fabricant;
        protected Constantes _constantes;

        /// <summary>
        /// Constructeur de GestionFilament
        /// </summary>
        public GestionFilament()
        {
            _vue = new Vue();
            _usager = new GestionUsager();
            _typeFilament = new FilamentType();
            _fabricant = new FabricantFilament();
            _constantes = new Constantes();
        }


        /// Question A3
        /// READ
        /// <summary>
        /// Lire les données dans la table Filament.
        /// </summary>
        public void Lire()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var filament = context.Filaments;
                foreach (var Filament in filament)
                {
                    Console.WriteLine("\nID :{0} \nCouleur: {1} \nLongueur initiale: {2} \nDate d'achat: {3} \nPropriétaire Id: {4} \nPrix: {5} \nPoid initial: {6} \nType de filament Id: {7} \nFabricant Id: {8}",
                                    Filament.ID,
                                    Filament.Couleur.Trim(),
                                    Filament.LongueurInitiale,
                                    Filament.DateAchat,
                                    Filament.ProprietaireId,
                                    Filament.Prix,
                                    Filament.PoidInitial,
                                    Filament.TypeFilamentId,
                                    Filament.FabricantId);

                    listeId.Add(Filament.ID);
                }
            }
            Console.Write("\nPressez Entrée ---> ");
            Console.ReadLine();
        }

        /// <summary>
        /// Obtient les Id des filaments dans la table Filament.
        /// </summary>
        /// <returns>Une liste avec les Id des filaments</returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var filaments = context.Filaments;
                foreach (var Filaments in filaments)
                {
                    Console.WriteLine("\nID: {0} | Couleur: {1} | Prix: {2} ",
                                    Filaments.ID,
                                    Filaments.Couleur.Trim(),
                                    Filaments.Prix
                                    );

                    listeId.Add(Filaments.ID);
                }
            }
            return listeId;
        }

        /// Question A3
        /// CREATE
        /// <summary>
        /// Permet de creer un nouveau filament dans la table Filament.
        /// </summary>
        public void Creer()
        {
            bool modifierFK = true;
            Filament filament = new Filament();
            filament = Informations(filament, modifierFK);
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    context.Filaments.Add(filament);
                    context.SaveChanges();
                }
            }
            catch { }
        }

        /// Question A3
        /// UPDATE
        /// <summary>
        /// Permet de modifier un filament dans la table Filament.
        /// </summary>
        public void Modifier()
        {
            bool modifierFK = false;
            int filamentId;
            string id = _constantes.filamentId;
            List<int> listeIdFilament = new List<int>();
            listeIdFilament = ObtenirListe();
            if (!_vue.ListeVide(listeIdFilament))
            {
                filamentId = _vue.ChoisirOption(listeIdFilament, id);
                Filament filamentModifie = new Filament();
                filamentModifie = Informations(filamentModifie, modifierFK);
                try
                {
                    using (var context = new TP2BD21282433Entities())
                    {
                        var filaments = context.Filaments
                                            .Where(c => c.ID == filamentId).ToList();
                        ;

                        var filament = filaments[0];
                        filament.DateAchat = filamentModifie.DateAchat;
                        filament.LongueurInitiale = filamentModifie.LongueurInitiale;
                        filament.PoidInitial = filamentModifie.PoidInitial;
                        filament.Prix = filamentModifie.Prix;
                        filament.ProprietaireId = filamentModifie.ProprietaireId;
                        filament.Couleur = filamentModifie.Couleur;
                        context.SaveChanges();
                    }
                }
                catch { }             
            }  
        }

        /// <summary>
        /// Prendre les informations sur le filament, les setter et retourner le filament.
        /// </summary>
        /// <param name="filament">Le filament a setter</param>
        /// <param name="modifierFK">Vrai s'il faut modifier les Foreign key</param>
        /// <returns>Retourne le filament avec les attributs setter</returns>
        public Filament Informations(Filament filament, bool modifierFK)
        {
            List<string> couleurs = new List<string>
            {
                "Bleu",
                "Rouge",
                "Orange",
                "Noir",
                "Blanc",
                "Gris",
                "Vert",
                "Rose",
                "Jaune"
            };
            string couleur;
            float longueurInitiale;
            DateTime dateAchat;
            int proprietaire;
            decimal prix;
            float poidInitial;
            int typeFilamentId;
            int fabricantId;
            _vue.AfficherCouleurs(couleurs);
            int choix;
            choix = _vue.ChoisirOption(new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8 }, _constantes.couleur);
            couleur = (couleurs.ElementAt(choix));
            do
            {
                longueurInitiale = _vue.InputFloat(_constantes.longueurInitiale);
            } while (longueurInitiale < 0);
            
            dateAchat = _vue.InputDate(_constantes.date);
            List<int> listeIdProprietaire = new List<int>();
            listeIdProprietaire = _usager.ObtenirListe();
            if (!_vue.ListeVide(listeIdProprietaire))
            {
                proprietaire = _vue.ChoisirOption(listeIdProprietaire, _constantes.proprietaire);
                do
                {
                    prix = _vue.InputDecimal(_constantes.prix);
                } while (prix < 0);

                do
                {
                    poidInitial = _vue.InputFloat(_constantes.poidInitial);
                } while (poidInitial < 0);

                if (modifierFK)
                {
                    List<int> listeIdTypeFilament = new List<int>();
                    List<int> listeIdFabricant = new List<int>();
                    listeIdTypeFilament = _typeFilament.ObtenirListe();
                    if (!_vue.ListeVide(listeIdTypeFilament))
                    {
                        typeFilamentId = _vue.ChoisirOption(listeIdTypeFilament, _constantes.typeFilament);
                        filament.TypeFilamentId = typeFilamentId;
                        listeIdFabricant = _fabricant.ObtenirListe();
                        if (!_vue.ListeVide(listeIdFabricant))
                        {
                            fabricantId = _vue.ChoisirOption(listeIdFabricant, _constantes.fabricant);
                            filament.FabricantId = fabricantId;
                        }
                    }  
                }
                filament.Couleur = couleur;
                filament.DateAchat = dateAchat;
                filament.LongueurInitiale = longueurInitiale;
                filament.PoidInitial = poidInitial;
                filament.Prix = prix;
                filament.ProprietaireId = proprietaire;
                
            }
            return filament;
        }
    }
}
