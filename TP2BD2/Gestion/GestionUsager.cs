﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP2BD2.CleeEtrangeres;

namespace TP2BD2
{
    /// <summary>
    /// Classe de gestion d'un usager permettant de (Créer, lire, modifier et effacer).
    /// </summary>
    class GestionUsager
    {
        protected Vue _vue;
        protected Salt _salt;
        protected Constantes _constantes;
        protected ImpressionClee _impression;
        protected FilamentClee _filament;

        /// <summary>
        /// Constructeur de GestionUsager
        /// </summary>
        public GestionUsager()
        {
            _vue = new Vue();
            _salt = new Salt();
            _constantes = new Constantes();
            _impression = new ImpressionClee();
            _filament = new FilamentClee();
        }

        /// Question A2
        /// READ
        /// <summary>
        /// Lire les données dans la table Usager.
        /// </summary>
        public void Lire()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var usager = context.Usagers;
                foreach (var Usager in usager)
                {
                    Console.WriteLine("\nID :{0} \nPrénom: {1} \nNom: {2} \nMot de passe: {3} \nSalt: {4}",
                                    Usager.ID,
                                    Usager.Nom.Trim(),
                                    Usager.Prenom.Trim(),
                                    Usager.MotDePasse.Trim(),
                                    Usager.Salt.Trim());

                    listeId.Add(Usager.ID);
                }
            }
            Console.Write("\nPressez Entrée ---> ");
            Console.ReadLine();
        }

        /// <summary>
        /// Obtient les Id des usagers dans la table Usager.
        /// </summary>
        /// <returns>Une liste avec les Id des usagers</returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var usagers = context.Usagers;
                foreach (var Usagers in usagers)
                {
                    Console.WriteLine("\nID: {0} | Nom: {1} | Prénom: {2} ",
                                    Usagers.ID,
                                    Usagers.Prenom.Trim(),
                                    Usagers.Nom.Trim()
                                    );

                    listeId.Add(Usagers.ID);
                }
            }
            return listeId;
        }

        /// Question A2
        /// CREATE
        /// <summary>
        /// Permet de créer un usager dans la table usager.
        /// </summary>
        public void Creer()
        {
            Usager usager = new Usager();
            usager = Informations(usager);
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    context.Usagers.Add(usager);
                    context.SaveChanges();
                }
            }
            catch { }    
        }

        /// Question A2
        /// UPDATE
        /// <summary>
        /// Permet de modifier un usager dans la table Usager.
        /// </summary>
        public void Modifier()
        {
            int usagerId;
            string id = _constantes.usagerId;
            List<int> listeIdUsager = new List<int>();
            listeIdUsager = ObtenirListe();
            if (!_vue.ListeVide(listeIdUsager))
            {
                usagerId = _vue.ChoisirOption(listeIdUsager, id);
                Usager usagerModifie = new Usager();
                usagerModifie = Informations(usagerModifie);
                try
                {
                    using (var context = new TP2BD21282433Entities())
                    {
                        var usagers = context.Usagers
                                            .Where(c => c.ID == usagerId).ToList();
                        ;
                        var usager = usagers[0];
                        usager.Prenom = usagerModifie.Prenom;
                        usager.Nom = usagerModifie.Nom;
                        usager.MotDePasse = usagerModifie.MotDePasse;
                        usager.Salt = usagerModifie.Salt;
                        context.SaveChanges();
                    }
                }
                catch { }   
            }   
        }

        /// <summary>
        /// Prendre les informations sur l'usager, les setter et retourner l'usager.
        /// </summary>
        /// <param name="usager">L'usager qu'il faut setter</param>
        /// <returns>L'usager avec les informations</returns>
        public Usager Informations(Usager usager)
        {
            string nom;
            string prenom;
            string motDePasse;
            string salt;
            nom = _vue.InputStringNonNull(_constantes.nom);
            prenom = _vue.InputStringNonNull(_constantes.prenom);
            var leMotDePasse = _vue.InputStringNonNull(_constantes.motDePasse);
            byte[] saltEnByte = _salt.CreerSalt();
            salt = Encoding.UTF8.GetString(saltEnByte);
            motDePasse = _salt.ComputeHash(leMotDePasse, "SHA256", saltEnByte);
            usager.Prenom = nom;
            usager.Nom = prenom;
            usager.MotDePasse = motDePasse;
            usager.Salt = salt;
            return usager;
        }


        /// Question 2
        /// DELETE
        /// <summary>
        /// Permet d'effacer un usager avec ses impressions, ses essaies, ses filaments et ses EssaieFilaments.
        /// </summary>
        /// <param name="usagerId">l'id de l'usager à effacer</param>
        private void Effacer(int usagerId)
        {
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    foreach (var filament in context.Filaments.Where(f => f.ProprietaireId == usagerId))
                    {
                        foreach (var essaieFilament in context.EssaieFilaments.Where(e => e.FilamentId == filament.ID))
                        {
                            context.EssaieFilaments.Remove(essaieFilament);
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch
            {
                // Ne rien faire
            }
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    foreach (var filament in context.Filaments.Where(f => f.ProprietaireId == usagerId))
                    {
                        context.Filaments.Remove(filament);
                    }
                    context.SaveChanges();
                }
            }
            catch
            {
                // Ne rien faire
            }
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    foreach (var impression in context.Impressions.Where(i => i.UsagerId == usagerId))
                    {
                        foreach (var essaie in context.Essaies.Where(e => e.ImpressionId == impression.ID))
                        {
                            context.Essaies.Remove(essaie);
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch
            {
                // Ne rien faire
            }
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    foreach (var impression in context.Impressions.Where(f => f.UsagerId == usagerId))
                    {
                        context.Impressions.Remove(impression);
                    }
                    context.SaveChanges();
                }
            }
            catch
            {
                // Ne rien faire
            }
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    var usagerAEffacer = context.Usagers.Find(usagerId);
                    context.Usagers.Remove(usagerAEffacer);
                    context.SaveChanges();
                }
            }
            catch
            {
                // Ne rien faire
            }
        }

        /// <summary>
        /// Permet de choisir quel usager effacer et afficher les autres entités qui seront effacées.
        /// </summary>
        public void ChoisirEffacer()
        {
            
            int choix;
            int usagerId;
            List<int> listeUsager = new List<int>();
            listeUsager = ObtenirListe();
            if (!_vue.ListeVide(listeUsager))
            {
                usagerId = _vue.ChoisirOption(listeUsager, _constantes.usagerId);
                choix = ChoixEffacer(usagerId);
                if (choix == 1)
                {
                    Effacer(usagerId);
                }
            } 
        }


        /// <summary>
        /// Permet de chosir si l'on veut vraiment effacer un usager ou non et d'afficher tout ce qui sera effacé.
        /// </summary>
        /// <param name="usagerId">L'id de l'usager à effacer</param>
        /// <returns>Le choix en input de l'utilisateur</returns>
        private int ChoixEffacer(int usagerId)
        {
            int choix;
            bool DoitAfficher = false;
            if (_impression.Existe(usagerId, false) || _filament.Existe(usagerId, false) || _filament.EssaieFilamentExiste(usagerId, false) || _impression.EssaieExiste(usagerId, false))
            {
                DoitAfficher = true;
            }
            return RetournerChoix(out choix, DoitAfficher, usagerId);
        }

        /// <summary>
        /// Retourne un choix correspondant à si on veut réellement effacer un usager.
        /// </summary>
        /// <param name="choix">Le choix que l'on va retourner</param>
        /// <param name="DoitAfficher">Si l'on doit afficher les relations</param>
        /// <param name="usagerId">L'id de l'usager à effacer</param>
        /// <returns></returns>
        private int RetournerChoix(out int choix, bool DoitAfficher, int usagerId)
        {
            if (DoitAfficher)
            {
                _impression.Existe(usagerId, true);
                _filament.Existe(usagerId, true);
                _filament.EssaieFilamentExiste(usagerId, true);
                _impression.EssaieExiste(usagerId, true);
                List<String> ChoixSuppression = new List<string>
                {
                    "\n1 - Effacer",
                    "0 - Annuler",
                    "\n"
                };
                _vue.AfficherChoix(ChoixSuppression);
                choix = _vue.ChoisirOption(new List<int> { 0, 1 }, _constantes.choix);
            }
            else
            {
                choix = 1;
            }
            return choix;
        }
    }
}
