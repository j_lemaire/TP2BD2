﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe de gestion d'un EssaieFilament permettant de (Créer, lire, modifier et effacer).
    /// </summary>
    class GestionEssaieFilament
    {
        protected GestionFilament _filament;
        protected Vue _vue;
        protected Constantes _constantes;

        public int essaieId;
        public int filamentId;
        public float quantiteUtilise;

        /// <summary>
        /// Constructeur de GestionEssaieFilament.
        /// </summary>
        public GestionEssaieFilament()
        {
            _filament = new GestionFilament();
            _vue = new Vue();
            _constantes = new Constantes();
        }

        /// Question B1 - EssaieFilament
        /// <summary>
        /// Permet de creer un EssaieFilament relié à un Essaie et un Filament.
        /// </summary>
        /// <param name="idEssaie">L'id de l'essaie à lier à L'essaieFilament et au Filament</param>
        public void Creer(int idEssaie)
        {
            EssaieFilament nouvelEsssaieFilament = new EssaieFilament();
            nouvelEsssaieFilament = Informations(nouvelEsssaieFilament, idEssaie);
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    var essaieFilament = new EssaieFilament
                    {
                        EssaieId = nouvelEsssaieFilament.EssaieId,
                        FilamentId = nouvelEsssaieFilament.FilamentId,
                        QteUtilise = nouvelEsssaieFilament.QteUtilise,
                    };
                    context.EssaieFilaments.Add(essaieFilament);
                    context.SaveChanges();
                }
            }
            catch
            {
                // Ignorer l'ajout...
            }   
        }

        /// <summary>
        /// Permet d'obtenir les informations du nouvel essaieFilament et les setter.
        /// </summary>
        /// <param name="nouvelEssaieFilament">Le nouvel essaieFilament à setter</param>
        /// <param name="idEssaie">L'id de l'essaie relié à L'essaieFilament</param>
        /// <returns>Le nouvel EssaieFilament</returns>
        public EssaieFilament Informations(EssaieFilament nouvelEssaieFilament, int idEssaie)
        {
            List<int> listeFilamentId = new List<int>();
            listeFilamentId = _filament.ObtenirListe();
            if (!_vue.ListeVide(listeFilamentId))
            {
                filamentId = _vue.ChoisirOption(listeFilamentId, _constantes.filamentId);
                essaieId = idEssaie;

                do
                {
                    quantiteUtilise = _vue.InputFloat(_constantes.quantiteFilament);
                } while (quantiteUtilise < 0);

                nouvelEssaieFilament.FilamentId = filamentId;
                nouvelEssaieFilament.EssaieId = essaieId;
                nouvelEssaieFilament.QteUtilise = quantiteUtilise;
            }
            return nouvelEssaieFilament;
        }

        /// <summary>
        /// Obtient les Id des filaments dans la table EssaieFilament.
        /// </summary>
        /// <returns>Une liste avec les Id des configurations physiques</returns>
        public List<int> ObtenirFilamentEssaie(int id)
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                IQueryable<EssaieFilament> essaieFilament = from c in context.EssaieFilaments
                                                            where c.EssaieId == id
                                                            select c;

                foreach (var filament in essaieFilament)
                {
                    Console.WriteLine("\nFilament ID: {0} | Essaie ID: {1}",
                            filament.FilamentId,
                            filament.EssaieId);
                    listeId.Add(filament.FilamentId);
                }
            }
            return listeId;
        }

        /// <summary>
        /// Permet de savoir si des essaieFilaments sont reliés à un filament.
        /// </summary>
        /// <param name="filamentId">L'id du filament</param>
        /// <returns>Vrai si un essaieFilament ou plus sont reliés au filament</returns>
        public bool Existe(int filamentId)
        {
            using (var context = new TP2BD21282433Entities())
            {
                var essaieFilament = context.EssaieFilaments
                                    .Where(c => c.FilamentId == filamentId).ToList();
                ;
                if (essaieFilament.Count > 0)
                {
                    Afficher(essaieFilament);
                    return true;
                }
                else return false;
            }
        }

        /// <summary>
        /// Permet d'afficher les essaieFilaments.
        /// </summary>
        /// <param name="essaieFilament">Une liste d'essaieFilaments</param>
        public void Afficher(List<EssaieFilament> essaieFilament)
        {
            Console.WriteLine("\n Les EssaieFilament suivants seront supprimés:");
            foreach (EssaieFilament ef in essaieFilament)
            {
                Console.WriteLine("\n Essaie ID: {0} | Filament ID: {1}",
                        ef.EssaieId,
                        ef.FilamentId);
            }
        }

        /// <summary>
        /// Permet de choisir quel filament effacer et afficher les autres entités qui seront effacées.
        /// </summary>
        public void ChoisirEffacer()
        {
            
            int choix;
            int filamentId;
            List<int> listeFilament = new List<int>();
            listeFilament = _filament.ObtenirListe();
            if (!_vue.ListeVide(listeFilament))
            {
                filamentId = _vue.ChoisirOption(listeFilament, _constantes.filamentId);
                choix = ChoixEffacer(filamentId);
                if (choix == 1)
                {
                    Effacer(filamentId);
                }
            }
        }

        /// Question A3
        /// DELETE
        /// <summary>
        /// Permet d'effacer un filament et les essaiefilaments reliés à ce filament.
        /// </summary>
        /// <param name="filamentId">L'id du filament à effacer</param>
        private void Effacer(int filamentId)
        {
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    foreach (var essaieFilament in context.EssaieFilaments.Where(f => f.FilamentId == filamentId))
                    {
                        context.EssaieFilaments.Remove(essaieFilament);
                    }
                    context.SaveChanges();
                }
            }
            catch
            {
                Console.Write("\nLa suppression ne s'est pas exécuté correctement !");
            }
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    var filamentAEffacer = context.Filaments.Find(filamentId);
                    context.Filaments.Remove(filamentAEffacer);
                    context.SaveChanges();
                }
            }
            catch
            {
                Console.Write("\nLa suppression ne s'est pas exécuté correctement !");
            }
        }

        /// <summary>
        /// Permet de chosir si l'on veut vraiment effacer un filament ou non.
        /// </summary>
        /// <param name="filamentId">L'id du filament à effacer</param>
        /// <returns>Le choix en input</returns>
        private int ChoixEffacer(int filamentId)
        {
            int choix;
            if (Existe(filamentId))
            {
                List<String> ChoixSuppression = new List<string>
                {
                    "1 - Effacer",
                    "0 - Annuler",
                };
                _vue.AfficherChoix(ChoixSuppression);
                choix = _vue.ChoisirOption(new List<int> { 0, 1 }, _constantes.choix);
            }
            else
            {
                choix = 1;
            }
            return choix;
        }
    }
}
