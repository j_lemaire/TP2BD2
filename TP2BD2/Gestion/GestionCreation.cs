﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2.Gestion
{
    /// <summary>
    /// Classe permettant de gérer une creation
    /// </summary>
    class GestionCreation
    {
        /// <summary>
        /// Permet d'obtenir une liste de créations
        /// </summary>
        /// <returns></returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var creations = context.Creations;
                foreach (var Creations in creations)
                {
                    Console.WriteLine(
                        "\nID: {0} " +
                        "\nNom: {1} " +
                        "\nURL Source: {2} " +
                        "\nDescription: {3}",
                                    Creations.ID,
                                    Creations.Nom.Trim(),
                                    Creations.URLSource.Trim(),
                                    Creations.Description.Trim()
                                    );

                    listeId.Add(Creations.ID);
                }
            }
            return listeId;
        }

        /// Question D1 
        /// Afficher filaments
        /// <summary>
        /// Permet d'afficher les filaments reliés à une création.
        /// </summary>
        /// <param name="creationId">L'id de la création</param>
        public void AfficherFilaments(int creationId)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\nLes Filaments suivants ont utilisés la création: ");
            using (var context = new TP2BD21282433Entities())
            {
                foreach (var impression in context.Impressions.Where(f => f.CreationId == creationId))
                {
                    foreach (var usager in context.Usagers.Where(e => e.ID == impression.UsagerId))
                    {
                       foreach (var filament in context.Filaments.Where(f => f.ProprietaireId == usager.ID))
                        {
                            Console.WriteLine(
                            "\nFilament ID: {0}" +
                            "\nCouleur: {1}" +
                            "\nLongueurInitiale: {2}" +
                            "\nDate d'achat: {3}" +
                            "\nPrix: {4}" +
                            "\nPoid intial: {5}"
                            ,
                            filament.ID,
                            filament.Couleur.Trim(),
                            filament.LongueurInitiale,
                            filament.DateAchat,
                            filament.Prix,
                            filament.PoidInitial);
                        }
                    }
                }
            }
            Console.WriteLine("Pressez entrée pour continuer ...");
            Console.ReadLine();
            Console.ResetColor();
        }

        /// Question D1
        /// Afficher impressions
        /// <summary>
        /// Permet d'afficher les impressions reliées à une création.
        /// </summary>
        /// <param name="creationId">L'id de la création</param>
        public void AfficherImpressions(int creationId)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nLes impressions suivantes ont utilisées la création: ");
            using (var context = new TP2BD21282433Entities())
            {
                var impression = context.Impressions
                                    .Where(c => c.CreationId == creationId).ToList();
                ;
                foreach (Impression imp in impression)
                {
                    Console.WriteLine(
                    "\nImpression ID: {0}" +
                    "\nNom: {1}" +
                    "\nDate d'impression: {2}" +
                    "\nCommentaire: {3}"
                     ,
                    imp.ID,
                    imp.Nom.Trim(),
                    imp.DateImpression,
                    imp.Commentaire.Trim());
                }
            }
            Console.WriteLine("\nPressez entrée pour continuer ...");
            Console.ReadLine();
            Console.ResetColor();
        }

        /// Question D1
        /// Afficher usagers
        /// <summary>
        /// Permet d'afficher les usagers reliés à une création.
        /// </summary>
        /// <param name="creationId">L'id de la création</param>
        public void AfficherUsagers(int creationId)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\nLes usagers suivants ont utilisés la création: ");
            using (var context = new TP2BD21282433Entities())
            {
                foreach (var impression in context.Impressions.Where(f => f.CreationId == creationId))
                {
                    foreach (var usager in context.Usagers.Where(e => e.ID == impression.UsagerId))
                    {
                        Console.WriteLine(
                        "\nUsager ID: {0}" +
                        "\nNom: {1}" +
                        "\nPrenom: {2}"
                        ,
                        usager.ID,
                        usager.Prenom.Trim(),
                        usager.Nom.Trim());
                    }
                }
            }
            Console.WriteLine("\nPressez entrée pour continuer ...");
            Console.ReadLine();
            Console.ResetColor();
        }

        /// Question D1
        /// Afficher essaies
        /// <summary>
        /// Permet d'afficher les essaies reliés à une création.
        /// </summary>
        /// <param name="creationId">L'id de l'essaie</param>
        public void AfficherEssaies(int creationId)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\nLes Essaies suivants ont utilisés la création: ");
            using (var context = new TP2BD21282433Entities())
            {
                foreach (var impression in context.Impressions.Where(f => f.CreationId == creationId))
                {
                    foreach (var essaie in context.Essaies.Where(e => e.ImpressionId == impression.ID))
                    {
                        Console.WriteLine(
                         "\nEssaie ID: {0}" +
                         "\nTemps de départ: {1}" +
                         "\nDurée réelle: {2}" +
                         "\nDurée estimée: {3}" +
                         "\nCommentaire: {4}" +
                         "\nPourcentage réussite: {5}"
                         ,
                         essaie.ID,
                         essaie.TempsDepart,
                         essaie.DureeReele,
                         essaie.DureeEstime,
                         essaie.Commentaire.Trim(),
                         essaie.PourcentageReussi);
                    }
                }
            }
            Console.WriteLine("\nPressez entrée pour continuer ...");
            Console.ReadLine();
            Console.ResetColor();
        }

        /// Question D1
        /// Afficher imprimantes
        /// <summary>
        /// Permet d'afficher les imprimantes reliées à une création.
        /// </summary>
        /// <param name="creationId">L'id de la création</param>
        public void AfficherImprimantes(int creationId)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("\nLes Imprimantes suivantes ont utilisées la création: ");
            using (var context = new TP2BD21282433Entities())
            {
                foreach (var impression in context.Impressions.Where(f => f.CreationId == creationId))
                {
                    foreach (var essaie in context.Essaies.Where(e => e.ImpressionId == impression.ID))
                    {
                        foreach (var configlogique in context.ConfigLogiques.Where(c => c.ID == essaie.ConfigLogiqueId))
                        {
                            foreach(var imprimante in context.Imprimantes.Where(i => i.ID == configlogique.ImprimanteId))
                            {
                                Console.WriteLine(
                                 "\nImprimante ID: {0}" +
                                 "\nDescription: {1}" +
                                 "\nPrix: {2}"
                                 ,
                                 imprimante.ID,
                                 imprimante.Description.Trim(),
                                 imprimante.Prix);
                            }
                        }
                    }
                }
            }
            Console.WriteLine("\nPressez entrée pour continuer ...");
            Console.ReadLine();
            Console.ResetColor();
        }

        /// Question D1
        /// Afficher Quantité
        /// <summary>
        /// Permet d'afficher la quantité de filament utilisé par les essaies reliés à une création.
        /// </summary>
        /// <param name="creationId">L'id de la création</param>
        public void AfficherQuantite(int creationId)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nLa création à utilisé ce nombre de filament par essaie: ");
            using (var context = new TP2BD21282433Entities())
            {
                foreach (var impression in context.Impressions.Where(f => f.CreationId == creationId))
                {
                    foreach (var essaie in context.Essaies.Where(e => e.ImpressionId == impression.ID))
                    {
                        foreach(var essaieFilament in context.EssaieFilaments.Where(v => v.EssaieId == essaie.ID))
                        {
                            Console.WriteLine(
                            "\nEssaie ID: {0}" +
                            "\nQuantitée de filament: {1}"
                            ,
                            essaieFilament.EssaieId,
                            essaieFilament.QteUtilise);
                        }
                    }
                }
            }
            Console.WriteLine("\nPressez entrée pour continuer ...");
            Console.ReadLine();
            Console.ResetColor();
        }
    }
}
