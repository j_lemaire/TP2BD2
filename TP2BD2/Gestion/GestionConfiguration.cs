﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2.Gestion
{
    class GestionConfiguration
    {
        protected ConfigurationLogique _configLogique;
        protected Vue _vue;
        protected Constantes _constantes;

        /// <summary>
        /// Constructeur de GestionConfiguration
        /// </summary>
        public GestionConfiguration()
        {
            _configLogique = new ConfigurationLogique();
            _vue = new Vue();
            _constantes = new Constantes();
        }

        /// <summary>
        /// Permet de choisir si l'on veut effacer une configuration et de choisir cette configuration.
        /// </summary>
        public void ChoisirEffacer()
        {          
            int configId;
            List<int> listeConfig = new List<int>();
            listeConfig = _configLogique.ObtenirListe();
            if (!_vue.ListeVide(listeConfig))
            {
                configId = _vue.ChoisirOption(listeConfig, _constantes.configLogiqueId);
                if (Existe(configId))
                {
                    Afficher(configId);
                }
                else Effacer(configId);
            }
        }

        /// Question C1
        /// Effacer
        /// <summary>
        /// Permet d'effacer une configuration logique qui n'est pas lié à un essaie
        /// </summary>
        /// <param name="configId">L'id de la configuration à effacer</param>
        private void Effacer(int configId)
        {
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    var configAEffacer = context.ConfigLogiques.Find(configId);
                    context.ConfigLogiques.Remove(configAEffacer);
                    context.SaveChanges();
                }
            }
            catch
            {
                Console.Write("\nLa suppression ne s'est pas exécuté correctement !");
            }
        }

        /// Question C1
        /// Afficher
        /// <summary>
        /// Permet d'afficher les autres entités qui effacées lors du Delete d'une configuration logique
        /// </summary>
        /// <param name="configId">L'id de la configuration</param>
        private void Afficher(int configId)
        {
            Console.Clear();
            using (var context = new TP2BD21282433Entities())
            {
                var essaie = context.Essaies
                                    .Where(c => c.ConfigLogiqueId == configId).ToList();
                ;

                Console.WriteLine(_constantes.configUtilise);
                foreach (Essaie e in essaie)
                {
                    Console.WriteLine(
                        "\nEssaie ID: {0} " +
                        "\nTemps de départ: {1}" +
                        "\nDurée réelle: {2}" +
                        "\nDurée estimée: {3}" +
                        "\nCommentaire: {4}" +
                        "\nPourcentage réussite: {5}",
                            e.ID,
                            e.TempsDepart,
                            e.DureeReele,
                            e.DureeEstime,
                            e.Commentaire.Trim(),
                            e.PourcentageReussi);
                }
            }
            Console.WriteLine("\nPressez sur entrée ---> ");
            Console.Read();
        }


        /// <summary>
        /// Permet de savoir si une configuration est liée à un essaie.
        /// </summary>
        /// <param name="configId">L'id de la configuration</param>
        /// <returns>Vrai si elle est liée</returns>
        private bool Existe(int configId)
        {
            using (var context = new TP2BD21282433Entities())
            {
                var essaie = context.Essaies
                                    .Where(c => c.ConfigLogiqueId == configId).ToList();
                ;
                if (essaie.Count > 0)
                {
                    return true;
                }
                else return false;
            }
        }
    }
}
