﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2BD2
{
    /// <summary>
    /// Classe de gestion d'une Imprimante permettant de (Créer, lire, modifier et effacer).
    /// </summary>
    class GestionImprimante
    {
        protected Vue _vue;
        protected ImprimanteModele _modele;
        protected Constantes _constantes;
        protected ConfigurationPhysique _configPhysique;
        protected ConfigurationLogique _configLogique;

        /// <summary>
        /// Constructeur de GestionImprimante
        /// </summary>
        public GestionImprimante()
        {
            _vue = new Vue();
            _modele = new ImprimanteModele();
            _constantes = new Constantes();
            _configPhysique = new ConfigurationPhysique();
            _configLogique = new ConfigurationLogique();
        }

        /// Question A1
        /// READ
        /// <summary>
        /// Lire les données dans la table Imprimante.
        /// </summary>
        public void Lire()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var imprimantes = context.Imprimantes;
                foreach (var Imprimantes in imprimantes)
                {
                    Console.WriteLine("\nID :{0} \nDescription: {1} \nPrix: {2} \nModele Id: {3}",
                                    Imprimantes.ID,
                                    Imprimantes.Description.Trim(),
                                    Imprimantes.Prix,
                                    Imprimantes.ModeleId);
                    listeId.Add(Imprimantes.ID);
                }
            }
            Console.Write("\nPressez Entrée ---> ");
            Console.ReadLine();
        }

        /// <summary>
        /// Obtient les Id des imprimantes dans la table Imprimante.
        /// </summary>
        /// <returns>Une liste avec les Id des imprimantes</returns>
        public List<int> ObtenirListe()
        {
            List<int> listeId = new List<int>();
            using (var context = new TP2BD21282433Entities())
            {
                var imprimantes = context.Imprimantes;
                foreach (var Imprimantes in imprimantes)
                {
                    Console.WriteLine("\nID: {0} | Description: {1}",
                                    Imprimantes.ID,
                                    Imprimantes.Description.Trim()
                                    );

                    listeId.Add(Imprimantes.ID);
                }
            }
            return listeId;
        }

        /// Question A1
        /// CREATE
        /// <summary>
        /// Création d'une imprimante dans la table imprimante.
        /// </summary>
        public void Creer()
        {
            bool modifierModele = true;
            Imprimante imprimante = new Imprimante();
            imprimante = Informations(imprimante, modifierModele);
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    context.Imprimantes.Add(imprimante);
                    context.SaveChanges();
                }
            }
            catch { } 
        }

        /// Question A1
        /// UPDATE
        /// <summary>
        /// Permet de modifier une imprimante dans la table imprimante.
        /// </summary>
        public void Modifier()
        {
            bool modifierModele = false;
            int imprimanteId;
            string id = _constantes.imprimanteId;
            List<int> listeIdImprimante = new List<int>();
            listeIdImprimante = ObtenirListe();
            if (!_vue.ListeVide(listeIdImprimante))
            {
                imprimanteId = _vue.ChoisirOption(listeIdImprimante, id);
                Imprimante imprimanteModifie = new Imprimante();
                imprimanteModifie = Informations(imprimanteModifie, modifierModele);
                try
                {
                    using (var context = new TP2BD21282433Entities())
                    {
                        var imprimantes = context.Imprimantes
                                            .Where(c => c.ID == imprimanteId).ToList();
                        ;

                        var imprimante = imprimantes[0];
                        imprimante.Description = imprimanteModifie.Description;
                        imprimante.Prix = imprimanteModifie.Prix;
                        context.SaveChanges();
                    }
                }
                catch { }            
            } 
        }

        /// <summary>
        /// Prendre les informations sur l'imprimante, les setter et retourner l'imprimante.
        /// </summary>
        /// <param name="imprimante">L'imprimante qu'il faut setter</param>
        /// <param name="modifierModele">L'imprimante avec les informations</param>
        /// <returns></returns>
        public Imprimante Informations(Imprimante imprimante, bool modifierModele)
        {
            string description;
            decimal prix;
            int modeleId;
            description = _vue.InputStringNonNull(_constantes.imprimanteDesc);
            do
            {
                prix = _vue.InputDecimal(_constantes.prix);
            } while (prix < 0);
            
            imprimante.Description = description;
            imprimante.Prix = prix;

            if (modifierModele)
            {
                List<int> listeIdModele = new List<int>();
                listeIdModele = _modele.ObtenirListe();
                if (!_vue.ListeVide(listeIdModele))
                {
                    modeleId = _vue.ChoisirOption(listeIdModele, _constantes.modeleId);
                    imprimante.ModeleId = modeleId;
                }
            }
            return imprimante;
        }

        /// Question A1
        /// EFFACER
        /// <summary>
        /// Permet d'effacer une imprimante dans la table imprimantes.
        /// </summary>
        /// <param name="imprimanteId"></param>
        private void Effacer(int imprimanteId)
        {
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    foreach (var configLogique in context.ConfigLogiques.Where(f => f.ImprimanteId == imprimanteId))
                    {
                        foreach (var essaie in context.Essaies.Where(e => e.ConfigLogiqueId == configLogique.ID))
                        {
                            foreach (var essaieFilament in context.EssaieFilaments.Where(a => a.EssaieId == essaie.ID))
                            {
                                context.EssaieFilaments.Remove(essaieFilament);
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch
            {
                Console.Write("\nLa suppression ne s'est pas exécuté correctement !");
            }
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    foreach (var configLogique in context.ConfigLogiques.Where(f => f.ImprimanteId == imprimanteId))
                    {
                        foreach (var essaie in context.Essaies.Where(e => e.ConfigLogiqueId == configLogique.ID))
                        {
                            context.Essaies.Remove(essaie);
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch
            {
                Console.Write("\nLa suppression ne s'est pas exécuté correctement !");
            }
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    foreach (var configLogique in context.ConfigLogiques.Where(f => f.ImprimanteId == imprimanteId))
                    {
                        context.ConfigLogiques.Remove(configLogique);
                    }
                    context.SaveChanges();
                }
            }
            catch
            {
                Console.Write("\nLa suppression ne s'est pas exécuté correctement !");
            }
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    foreach (var configphysique in context.ConfigPhysiques.Where(c => c.ImprimanteId == imprimanteId))
                    {
                        ConfigPhysique cp = context.ConfigPhysiques.Find(configphysique.ID);
                        foreach (Buse b in cp.Buses.ToList())
                        {
                            cp.Buses.Remove(b);   // dissocie les config de la buse
                        }
                        context.ConfigPhysiques.Remove(cp);
                    }
                    context.SaveChanges();
                }
            }
            catch
            {
                Console.Write("\nLa suppression ne s'est pas exécuté correctement !");
            }       
            try
            {
                using (var context = new TP2BD21282433Entities())
                {
                    var imprimanteAEffacer = context.Imprimantes.Find(imprimanteId);
                    context.Imprimantes.Remove(imprimanteAEffacer);
                    context.SaveChanges();
                }
            }
            catch
            {
                Console.Write("\nLa suppression ne s'est pas exécuté correctement !");
            }
        }

        /// <summary>
        /// Permet de choisir quelle imprimante effacer
        /// </summary>
        public void ChoisirEffacer()
        {
            
            int choix;
            int imprimanteId;
            List<int> listeImprimante = new List<int>();
            listeImprimante = ObtenirListe();
            if (!_vue.ListeVide(listeImprimante))
            {
                imprimanteId = _vue.ChoisirOption(listeImprimante, _constantes.imprimanteId);
                choix = ChoixEffacer(imprimanteId);
                if (choix == 1)
                {
                    Effacer(imprimanteId);
                }
            }
        }

        /// <summary>
        /// Permet de chosir si l'on veut vraiment effacer une imprimante ou non et d'afficher tout ce qui sera effacé.
        /// </summary>
        /// <param name="imprimanteId">L'id de l'imprimante à effacer</param>
        /// <returns>Le choix en input de l'utilisateur</returns>
        private int ChoixEffacer(int imprimanteId)
        {
            int choix;
            bool DoitAfficher = false;
            if (_configPhysique.Existe(imprimanteId, false) || _configLogique.Existe(imprimanteId, false) || _configLogique.EssaieExiste(imprimanteId, false))
            {
                DoitAfficher = true;
            }
            return RetournerChoix(out choix, DoitAfficher, imprimanteId);
        }

        /// <summary>
        /// Choix pour savoir si l'on veut réellement effacer une imprimante.
        /// </summary>
        /// <param name="choix">Le choix qui sera retourné</param>
        /// <param name="DoitAfficher">Si l'on doit afficher les relations</param>
        /// <param name="imprimanteId">L'id de l'imprimante à effacer</param>
        /// <returns></returns>
        private int RetournerChoix(out int choix, bool DoitAfficher, int imprimanteId)
        {
            if (DoitAfficher)
            {
                _configPhysique.Existe(imprimanteId, true);
                _configLogique.Existe(imprimanteId, true);
                _configLogique.EssaieExiste(imprimanteId, true);
                List<String> ChoixSuppression = new List<string>
                {
                    "1 - Effacer",
                    "0 - Annuler",
                    "\n"
                };
                _vue.AfficherChoix(ChoixSuppression);
                choix = _vue.ChoisirOption(new List<int> { 0, 1 }, _constantes.choix);
            }
            else
            {
                choix = 1;
            }
            return choix;
        }
    }
}
