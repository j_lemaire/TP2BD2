 --efface les tables intermédiaires afin d'éviter les conflits de FK NO ACTION
 
 DELETE FROM EssaieFilament
 DELETE FROM Essaie
 DELETE FROM ConfigPhysiqueBuse
 DELETE FROM BuseTypeFilament

 

 --Fabricant
 DELETE FROM Fabricant;
 INSERT INTO Fabricant VALUES
 ('3D solutech'),
 ('MG Chemical'),
 ('MeltInk'),
 ('AMZ3D'),
 ('LOTW'),
 ('Mech Solution'),
 ('Anet'),
 ('SnapMaker'),
 ('Prusia')
 
 -- Type de Filament
 DELETE FROM TypeFilament
 INSERT INTO TypeFilament VALUES
 ('PLA', 1.75),
 ('ABS', 1.75),
 ('PETG', 1.75),
 ('HIPS', 1.75),
 ('Wood-PLA', 1.75)
 
 
 -- Les usagers
 DELETE FROM Usager
 DECLARE @salt as CHAR(64) = '/$%DTEG/RFGDS%$HBN$%%/$'
 
 INSERT INTO Usager VALUES('Benoit', 'Desrosiers', HASHBYTES('SHA2_256','password'+@salt),@salt)
 DECLARE @IdUsagerBenoit AS INT = SCOPE_IDENTITY();

 SET @salt = '/$%DTEG/ds3345%$HBN$%%/$'
 INSERT INTO Usager VALUES ('Joe', 'Bine', HASHBYTES('SHA2_256','password'+@salt),@salt)
 DECLARE @IdUsagerJoe AS INT = SCOPE_IDENTITY();

 SET @salt = '/53gsg$/%dsfDS%$HBN$%%/$'
 INSERT INTO Usager VALUES ('Bob', 'Binette', HASHBYTES('SHA2_256','password'+@salt),@salt)
 DECLARE @IdUsagerBob AS INT = SCOPE_IDENTITY();

 -- Les modèles d'imprimantes
 DELETE FROM ModeleImprimante
 Declare @fabricantAnet as INT;
 SET @fabricantAnet = (
 SELECT ID FROM Fabricant WHERE Nom = 'Anet' )
 Declare @fabricantSnap as INT;
 SET @fabricantSnap = (
 SELECT ID FROM Fabricant WHERE Nom = 'SnapMaker' )
 Declare @fabricantPrusia as INT;
 SET @fabricantPrusia = (
 SELECT ID FROM Fabricant WHERE Nom = 'Prusia' )
 
 INSERT INTO ModeleImprimante VALUES ('Anet A8', 450, @fabricantAnet)
 DECLARE @IdModeleAnet AS INT = SCOPE_IDENTITY();
 INSERT INTO ModeleImprimante VALUES ('Snapmaker 3 in 1', 800, @fabricantSnap)
 DECLARE @IdModeleSnap AS INT = SCOPE_IDENTITY();
 INSERT INTO ModeleImprimante VALUES ('MK 3', 900, @fabricantPrusia)
 DECLARE @IdModelePrusia AS INT = SCOPE_IDENTITY();



 --Les imprimantes
 DELETE FROM Imprimante
 INSERT INTO Imprimante VALUES ('Anet A8 de Benoit', 250, @IdModeleAnet)
 DECLARE @IdImpA8 AS INT = SCOPE_IDENTITY();
 INSERT INTO Imprimante VALUES ('Snapmaker de Benoit', 800, @IdModeleSnap)
 DECLARE @IdImpSnap AS INT = SCOPE_IDENTITY();
 INSERT INTO Imprimante VALUES ('Imprimante du club', 749, @IDModelePrusia)
 DECLARE @IdImpClub AS INT = SCOPE_IDENTITY();

 --Les Créations 
 DELETE FROM Creation
 INSERT INTO Creation VALUES
 ('Baby Groot', 'https://www.thingiverse.com/thing:2014307', 'Baby groot assis'),
 ('Arc Mini', 'https://www.thingiverse.com/thing:2764962', 'Arc à poulie miniature'), 
 ('Mannequin', 'https://www.thingiverse.com/thing:2750463', 'Mannequin grandeur carte de crédit')

 --Les Buses
 DELETE FROM Buse

 Declare @fabricantBuse1 as INT = (
 SELECT ID FROM Fabricant WHERE Nom = 'LOTW' )
 
 INSERT INTO Buse VALUES
 ('Extruder 1.75-0.4', 1, @fabricantBuse1, 1.75, 0.4) 
 DECLARE @IdLaBuse AS INT = SCOPE_IDENTITY();
 
 --Les Configuration Physiques
 DELETE FROM ConfigPhysique
 INSERT INTO ConfigPhysique VALUES (@idImpA8, 1, 'Config de Base, 1 buse 1.75-0.4')
 DECLARE @IdConfigPhysique1 AS INT = SCOPE_IDENTITY(); 
 INSERT INTO ConfigPhysique VALUES (@idImpA8, 0, 'Config extreme, 1 buse, 3.00-1')
 DECLARE @IdConfigPhysique2 AS INT = SCOPE_IDENTITY(); 
 INSERT INTO ConfigPhysique VALUES (@idImpSnap, 1, 'Config de Base')
 DECLARE @IdConfigPhysique3 AS INT = SCOPE_IDENTITY(); 
 INSERT INTO ConfigPhysique VALUES (@idImpClub, 1, 'Config de Base')
 DECLARE @IdConfigPhysique4 AS INT = SCOPE_IDENTITY(); 
 
 --Le lien entre les config physique et les buses.
 --Pour l'instant, la buse est plus un modèle de buse, les imprimantes ont donc toutes le même modèle, 
 
 INSERT INTO ConfigPhysiqueBuse VALUES
 (@IdConfigPhysique1, @IdLaBuse),
 (@IdConfigPhysique2, @IdLaBuse),
 (@IdConfigPhysique3, @IdLaBuse),
 (@IdConfigPhysique4, @IdLaBuse)
 
 --Les configurations logiques
 DELETE FROM ConfigLogique
 INSERT INTO ConfigLogique VALUES (@IdImpA8, 0.1, 0.5, 'qualité',  1)
 DECLARE @IdConfigLogique1 AS INT = SCOPE_IDENTITY(); 
 INSERT INTO ConfigLogique VALUES (@IdImpA8, 0.2, 0.1, 'rapide', 1)
 DECLARE @IdConfigLogique2 AS INT = SCOPE_IDENTITY(); 
 INSERT INTO ConfigLogique VALUES (@IdImpA8, 0.2, 0.5, 'intermédiaire', 1)
 DECLARE @IdConfigLogique3 AS INT = SCOPE_IDENTITY();  

--Les Impressions
DELETE FROM Impression
INSERT INTO Impression VALUES
(@IdUsagerBenoit, 'Baby Groot', (SELECT ID FROM Creation WHERE Nom = 'Baby Groot'), '20180120', 'Pour le concours')
DECLARE @IdImpression1 AS INT = SCOPE_IDENTITY(); 

INSERT INTO Impression VALUES
(@IdUsagerBenoit, 'Mini Arc', (SELECT ID FROM Creation WHERE Nom = 'Arc Mini'), '20180220', 'Pour le Club d arc')
DECLARE @IdImpression2 AS INT = SCOPE_IDENTITY(); 

INSERT INTO Impression VALUES
(@IdUsagerBenoit, 'Mannequin logo de la technique', (SELECT ID FROM Creation WHERE Nom = 'Mannequin'), '20180220', 'Modifié pour avoir le logo de la techn info')
DECLARE @IdImpression3 AS INT = SCOPE_IDENTITY(); 
 
 
--Les Essaies
INSERT INTO Essaie VALUES ('20180217 10:30:00', '30', '32', 'Très bien imprimé', @IdConfigLogique1, @IdImpression1, 100, @IdConfigPhysique1)
DECLARE @IdEssaie1 AS INT = SCOPE_IDENTITY(); 
INSERT INTO Essaie VALUES ('20180217 9:30:00', '30', '32', 'Problème de dé laminage', @IdConfigLogique2, @IdImpression1, 100, @IdConfigPhysique1)
DECLARE @IdEssaie2 AS INT = SCOPE_IDENTITY(); 
INSERT INTO Essaie VALUES ('20180217 11:30:00', '30', '32', 'Belle qualité', @IdConfigLogique1, @IdImpression2, 100, @IdConfigPhysique1)
DECLARE @IdEssaie3 AS INT = SCOPE_IDENTITY(); 


--Les Filaments
DELETE FROM Filament
DECLARE @IdTypeFilamentPLA AS INT = (SELECT ID FROM TypeFilament WHERE Materiel='PLA')
DECLARE @IdFournisseur1 AS INT = (SELECT ID FROM Fabricant WHERE Nom = '3D solutech')

INSERT INTO Filament VALUES ('blanc', 250, '20180110', @IdUsagerBenoit, 25.45, 1,@IdTypeFilamentPLA, @IdFournisseur1)
DECLARE @IdFilamentBlanc AS INT = SCOPE_IDENTITY(); 
INSERT INTO Filament VALUES ('noir', 250, '20180110', @IdUsagerBenoit, 25.45, 1,@IdTypeFilamentPLA, @IdFournisseur1)
DECLARE @IdFilamentNoir AS INT = SCOPE_IDENTITY(); 
INSERT INTO Filament VALUES ('bleu', 250, '20180110', @IdUsagerBenoit, 25.45, 1,@IdTypeFilamentPLA, @IdFournisseur1)
DECLARE @IdFilamentBleu AS INT = SCOPE_IDENTITY(); 

--Les filaments utilisés par les essaies
INSERT INTO EssaieFilament VALUES 
(@IdEssaie1, @IdFilamentBlanc, 3),
(@IdEssaie2, @IdFilamentBlanc, 3),
(@IdEssaie3, @IdFilamentNoir, 3)

--Les types de filament utilisable par les buses
INSERT INTO BuseTypeFilament VALUES
(@IdLaBuse, (SELECT id FROM TypeFilament WHERE Materiel = 'PLA')), 
(@IdLaBuse, (SELECT id FROM TypeFilament WHERE Materiel = 'PETG')), 
(@IdLaBuse, (SELECT id FROM TypeFilament WHERE Materiel = 'HIPS')) 


 