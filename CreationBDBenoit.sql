IF OBJECT_ID('dbo.BuseTypeFilament') IS NOT NULL DROP TABLE dbo.BuseTypeFilament;
IF OBJECT_ID('dbo.EssaieFilament') IS NOT NULL DROP TABLE dbo.EssaieFilament;
IF OBJECT_ID('dbo.Filament') IS NOT NULL DROP TABLE dbo.Filament;
IF OBJECT_ID('dbo.TypeFilament') IS NOT NULL DROP TABLE dbo.TypeFilament;
IF OBJECT_ID('dbo.Essaie') IS NOT NULL DROP TABLE dbo.Essaie;
IF OBJECT_ID('dbo.ConfigPhysiqueBuse') IS NOT NULL DROP TABLE dbo.ConfigPhysiqueBuse;
IF OBJECT_ID('dbo.ConfigLogique') IS NOT NULL DROP TABLE dbo.ConfigLogique;
IF OBJECT_ID('dbo.ConfigPhysique') IS NOT NULL DROP TABLE dbo.ConfigPhysique;
IF OBJECT_ID('dbo.Imprimante') IS NOT NULL DROP TABLE dbo.Imprimante;
IF OBJECT_ID('dbo.Impression') IS NOT NULL DROP TABLE dbo.Impression;
IF OBJECT_ID('dbo.Usager') IS NOT NULL DROP TABLE dbo.Usager;
IF OBJECT_ID('dbo.Creation') IS NOT NULL DROP TABLE dbo.Creation;
IF OBJECT_ID('dbo.ModeleImprimante') IS NOT NULL DROP TABLE dbo.ModeleImprimante;
IF OBJECT_ID('dbo.Buse') IS NOT NULL DROP TABLE dbo.Buse;
IF OBJECT_ID('dbo.Fabricant') IS NOT NULL DROP TABLE dbo.Fabricant;

--la table pour le log du trigger
IF OBJECT_ID('dbo.logtable') IS NOT NULL DROP TABLE dbo.logtable;

-- L'information sur le fabricant des imprimantes, des buses, et des filaments.
CREATE TABLE Fabricant (
ID INT  NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_Fabricant_ID PRIMARY KEY,
Nom CHAR(255) NOT NULL,

)
GO


CREATE TABLE Buse (
ID INT  NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_Buse_ID PRIMARY KEY,
Description CHAR(255),
Prix DECIMAL(17,2),
FabricantId INT CONSTRAINT FK_Buse_FabricantId_ID FOREIGN KEY (FabricantId)
		REFERENCES dbo.Fabricant (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
DiametreIn float(10) NOT NULL,
DiametreOut float(10) NOT NULL

)
GO

-- Plusieurs imprimantes peuvent avoir le même modèle. 
-- Le prix est une référence seulement. Le vrai prix d'achat est avec l'imprimante
CREATE TABLE ModeleImprimante (
ID INT  NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_ModeleImprimante_ID PRIMARY KEY,
Description CHAR(255) NOT NULL,
Prix DECIMAL(17,2),
FabricantId INT CONSTRAINT FK_ModeleImprimante_FabricantId FOREIGN KEY (FabricantId)
		REFERENCES dbo.Fabricant (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
)
GO

-- Les plans que l'on veux imprimer. 
-- Le mot Plan étant un mot réservé, j'ai du utiliser Creation
CREATE TABLE Creation (
ID INT  NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_Plan_ID PRIMARY KEY,
Nom CHAR(255) NOT NULL,
URLSource VARCHAR(1000),
Description VARCHAR(1000),
)
GO

-- Les utilisateurs du système
-- Le mot de passe est encrypté avec le salt. 
CREATE TABLE Usager (
ID INT  NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_Usager_ID PRIMARY KEY,
Nom CHAR(255) NOT NULL,
Prenom CHAR(255) NOT NULL,
MotDePasse CHAR(255) NOT NULL,
Salt CHAR(255) NOT NULL
)
GO

-- L'impression d'une création par un utilisateur à une date donnée. 
-- Le commentaire sert à indiquer si cette impression a donné de bons résultats.
-- Plusieurs essaies pourront être fait pour imprimer cette création. 
CREATE TABLE Impression (
ID INT  NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_Impression_ID PRIMARY KEY,
UsagerId INT CONSTRAINT FK_Impression_UsagerId_ID FOREIGN KEY (UsagerId)
		REFERENCES dbo.Usager (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
Nom CHAR(255),
CreationId  INT CONSTRAINT FK_Impression_CreationId_ID FOREIGN KEY (CreationId)
		REFERENCES dbo.Creation (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
DateImpression DateTime NOT NULL,
Commentaire VARCHAR(1000),
)
GO


-- Les imprimantes 
-- Le prix est le vrai prix d'achat.
CREATE TABLE Imprimante (
ID INT  NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_Imprimante_ID PRIMARY KEY,
Description CHAR(255) NOT NULL, 
Prix DECIMAL(17,2) NOT NULL,
ModeleId INT 
  CONSTRAINT FK_Imprimante_ModeleId_ID FOREIGN KEY (ModeleId)
		REFERENCES dbo.ModeleImprimante (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,	
)
GO

-- La configuration possibles de buses (ou tout autre futur equipement physique) pouvant être installé sur une imprimante.
-- Active indique laquelle de ces configurations est présentement utilisée. 
-- Il ne doit y avoir qu'une seule configuration active par imprimante. 
-- Une de ces configurations est sélectionné pour une essaie. Dès qu'un essaie est associé, il doit 
-- être impossible d'effacer la configuration (doit être implanté par du code)
CREATE TABLE ConfigPhysique (
ID INT  NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_ConfigPhysique_ID PRIMARY KEY,
ImprimanteId INT 
	CONSTRAINT FK_ConfigPhysique_ImprimanteId_ID FOREIGN KEY (ImprimanteId)
		REFERENCES dbo.Imprimante (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
Active BIT NOT NULL,  -- est-ce la config active pour cette imprimante. Devrait être unique par imprimante. 
Description VARCHAR(1000),
)
GO

-- Les possibles configurations de paramètres logiques d'une imprimante. 
-- Une configuration logique peut être utilisée par plusieurs essaies, mais 
-- dès qu'un essai est associé, il doit être impossible d'effacer la config (doit être implanté par du code)
CREATE TABLE ConfigLogique (
ID INT  NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_ConfigLogique_ID PRIMARY KEY,
ImprimanteId INT 
	CONSTRAINT FK_ConfigLogique_ImprimanteId FOREIGN KEY (ImprimanteId)
		REFERENCES dbo.Imprimante (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
EpaisseurCouche FLOAT(10),
Remplissage  FLOAT(10),
Description CHAR(255),
DeBase BIT NOT NULL,  -- est-ce une configuration de base de l'imprimante
)
GO


-- L'association d'une buse à une configuration physique.
/*
Les contraintes de DELETE/UPDATE sont mises à NO ACTION afin de prévenir une boucle de FK. 
Il est plutot rare qu'on veuille effacer une ConfigPhysique ou une Buse. Si c'est le cas, on devra effacer
cette table manuellement en premier 
*/
CREATE TABLE ConfigPhysiqueBuse (
ConfigPhysiqueId INT  NOT NULL 
	CONSTRAINT FK_ConfigPhysiqueBuse_ConfigPhysiqueId FOREIGN KEY (ConfigPhysiqueId)
		REFERENCES dbo.ConfigPhysique (ID)
		ON DELETE NO ACTION  
		ON UPDATE NO ACTION,

BuseId INT NOT NULL 
	CONSTRAINT FK_ConfigPhysiqueBuse_BuseId FOREIGN KEY (BuseId)
		REFERENCES dbo.Buse (ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
		 
CONSTRAINT PK_ConfigPhysique_ConfigPhysiqueId_BuseId PRIMARY KEY 
	(ConfigPhysiqueID, 
	 BuseID
)
)

-- Les tentatives d'impression et les configurations utilisées, ainsi que les résultats de chacune d'elles. 
CREATE TABLE Essaie  (
ID INT NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_Essaie_ID PRIMARY KEY,
TempsDepart DATETIME NOT NULL,
DureeReele INT,
DureeEstime INT, 
Commentaire VARCHAR(1000),
ConfigLogiqueId INT  
	CONSTRAINT FK_Essai_ConfigLogiqueId FOREIGN KEY (ConfigLogiqueId)
		REFERENCES dbo.ConfigLogique (ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
ImpressionId INT
	CONSTRAINT FK_Essai_ImpressionId FOREIGN KEY (ImpressionId)
		REFERENCES dbo.Impression (ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
PourcentageReussi INT,
ConfigPhysiqueId INT
	CONSTRAINT FK_Essai_ConfigPhysiqueId FOREIGN KEY (ConfigPhysiqueId)
		REFERENCES dbo.ConfigPhysique (ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,	
	
)


-- Les types de filament (PLA, PELG, ABS ...)
CREATE TABLE TypeFilament (
ID INT NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_TypeFilament_ID PRIMARY KEY, 
Materiel CHAR(30),
Diametre FLOAT(10),

)

-- Les bobines de filaments. 
CREATE TABLE Filament (
ID INT NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_Filament_ID PRIMARY KEY,
Couleur CHAR(255) NOT NULL,
LongueurInitiale FLOAT(10) NOT NULL, 
DateAchat DATE,
ProprietaireId INT
	CONSTRAINT FK_Filament_ProprietaireId FOREIGN KEY (ProprietaireId)
		REFERENCES dbo.Usager (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
Prix DECIMAL(5,2),
PoidInitial FLOAT(10),
TypeFilamentId INT
	CONSTRAINT FK_Filament_TypeFilamentId FOREIGN KEY (TypeFilamentId)
		REFERENCES dbo.TypeFilament (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
FabricantId INT CONSTRAINT FK_Filament_FabricantId FOREIGN KEY (FabricantId)
		REFERENCES dbo.Fabricant (ID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,

)

-- L'association entre les essaies et les filaments. 
-- Le delete est NO ACTION afin de briser la boucle
CREATE TABLE EssaieFilament (
EssaieId INT  NOT NULL 
	CONSTRAINT FK_EssaieFilament_EssaieId FOREIGN KEY (EssaieId)
		REFERENCES dbo.Essaie (ID)
		ON DELETE NO ACTION  
		ON UPDATE NO ACTION,

FilamentId INT NOT NULL 
	CONSTRAINT FK_EssaieFilament_FilamentId FOREIGN KEY (FilamentId)
		REFERENCES dbo.Filament (ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
QteUtilise FLOAT(10) NOT NULL,
		 
CONSTRAINT PK_EssaieFilameentId_EssaieId_FilamentID PRIMARY KEY 
	(EssaieId, 
	 FilamentId
	)
)

-- Les types de filaments pouvant être utilisés sur une buse. 
CREATE TABLE BuseTypeFilament (
BuseId INT  NOT NULL 
	CONSTRAINT FK_BuseTypefilament_BuseId FOREIGN KEY (BuseId)
		REFERENCES dbo.Buse (ID)
		ON DELETE NO ACTION  
		ON UPDATE NO ACTION,

TypeFilamentId INT NOT NULL 
	CONSTRAINT FK_BuseTypeFilament_TypeFilamentId FOREIGN KEY (TypeFilamentId)
		REFERENCES dbo.TypeFilament (ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
		 
CONSTRAINT PK_BuseTypeFilament_BuseId_TypeFilamentId PRIMARY KEY 
	(BuseId, 
	 TypeFilamentId
	)
)

--Les logs enregistrées par la procédure stockée iduTypeFilament lors de changements sur TypeFilament
CREATE TABLE LogTable (
ID INT NOT NULL IDENTITY(1,1)
	CONSTRAINT PK_LogTable_ID PRIMARY KEY,
DateEvenement DATETIME NOT NULL,
Evenement CHAR(10) NOT NULL,
IdOriginal INT NOT NULL,
MaterielOriginal CHAR(30),
DiametreOriginal FLOAT(10),
MaterielNouveau CHAR(30),
DiametreNouveau FLOAT(10)
)
	
GO
